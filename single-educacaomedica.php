<?php 
	$unixtimestamp = strtotime( get_field('data_de_inicio') );
	$current_month = date_i18n( "F/Y", $unixtimestamp);
	$currentDate = date_i18n( "d", $unixtimestamp);
?>

<?php get_template_part( 'components/header' ); ?>
	<div class="container-page post-inner event-inner">
		<div class="header-inner">
			<button class="btn-back"></button>
			<div class="breadcrumbs">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
		<div class="post">
			<?php if ( has_post_thumbnail()) : ?>
				<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
				<img class="thumb" src="<?php echo $thumbnail[0]; ?>"></img>
				<?php else : ?>
				<div class="thumb no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
			<?php endif; ?>
			<div class="content">
				<div class="header-content">
					<h2 class="title"><?php echo the_title() ?></h2>
					<span class="publication">
						<span class="description-date">Publicado há:</span>
						<time class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp')) ?></time>
					</span>
				</div>
				<div class="content-event">
					<div class="text">
						<?php echo $content = apply_filters ("the_content", $post->post_content); ?>
					</div>
					<div class="infos">
						<div class="card-dates">
							<div class="header-card">
								<span><?php echo $current_month ?></span>
							</div>
							<h3 class="title-day"><?php echo $currentDate ?></h3>

							<ul class="horarios">
								<li class="horario">
									<div class="icon"></div>
									<div class="content-horario">
										<span class="description">Carga horaria</span>
										<span class="title"><?php echo the_field('carga_horaria') ?>h</span>
									</div>
								</li>
								<li class="horario">
									<div class="icon"></div>
									<div class="content-horario">
										<!-- horarios -->
										<?php
											$horarios = get_field('horarios');
											$inicio = $horarios['inicio'];
											$fim = $horarios['fim'];
										?>
										<span class="description">Horarios</span>
										<span class="title"><?php echo $inicio ?> as <?php echo $fim ?></span>
									</div>
								</li>
							</ul>
						</div>
						<div class="local">
							<div class="group">
								<div class="header-local">Local</div>
								<div class="wrap-local">
									<div class="icon"></div>
									<div class="content-local">
										<span><?php echo the_field('local') ?></span>
									</div>
								</div>
							</div>
							<div class="map"></div>
						</div>
					</div>
					
					<?php if( have_rows('topicos') ): ?>

						<div class="row">

						<?php while( have_rows('topicos') ): the_row();
							$titulo_topico = get_sub_field('titulo_topico');
							$texto_topico = get_sub_field('texto_topico');
						?>

							<span class="title-row"><?php echo $titulo_topico ?></span>
							<div class="text-row">
								<?php echo $texto_topico ?>
							</div>

						<?php endwhile; ?>

						</div>

					<?php endif; ?>

					<div class="investimento">
						<span class="title">Seu Investimento</span>
						<h3 class="valor">R$ <?php echo the_field('investimento') ?></h3>
						<span class="description">Invista no seu futuro</span>
					</div>
				</div>
				<div class="shared">
					<!-- <div class="newsletter">
						<div class="icon"></div>
						<h3 class="title">Receba as novas noticias no seu email</h3>
						<p class="description">Você tambem irá receber novidades de todo site</p>

						<form class="form" action="Post">
							<input type="email" class="input-email" placeholder="E-mail">
							<button class="btn-submit">Assinar</button>
						</form>

					</div> -->
					<div class="redes">
						<ul class="list">
							<li class="item facebook"></li>
							<li class="item linkedin"></li>
							<li class="item twitter"></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="relacionadas">
			<?php
				$args_posts = array(
					'post_type' => array('educacaomedica'),
					'posts_per_page' => 2,
					'orderby' => 'rand'
				);

				$query_noticias = get_posts( $args_posts );

				query_posts( $args_posts );
			?>
			<ul class="list-news list">
				<?php 
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							get_template_part( 'components/news/new-card' );
						endwhile;
					endif;
				?>
			</ul>
		</div>
	</div>
<?php get_template_part( 'components/footer' ); ?>
<script type="text/javascript">
	
</script>