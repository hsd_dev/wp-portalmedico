<?php
    /**
    * template name: home
    */
?>

<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/styles/dist/home.css">

<?php get_template_part( 'components/header' ); ?>

<div class="home">
	<?php get_template_part( 'components/banners/index' ); ?>
	<?php get_template_part( 'components/downloads/index' ); ?>
	<?php get_template_part( 'components/events/index' ); ?>
	<?php get_template_part( 'components/news/index' ); ?>
	<?php get_template_part( 'components/news/semjaleco' ); ?>
</div>

<?php get_template_part( 'components/footer' ); ?>