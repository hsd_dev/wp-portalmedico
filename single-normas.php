<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/styles/dist/normas.css">
<?php get_template_part( 'components/header' ); ?>
	<div class="section-page normas">
		<div class="header-page">
			<div class="content-header">
				<?php $page = get_page_by_title( 'Normas e diretrizes' ); ?>
				<h2 class="title"><?php echo get_the_title( $page->ID ) ?></h2>
				<p><?php echo get_post_field('post_content', $page->ID) ?></p>
			</div>
			<?php
				echo wp_nav_menu( array(
					'menu'     => 'principal',
					'sub_menu' => true
				));
			?>
		</div>
		<div class="tab-content">
			<div class="tab-section active">
				<?php if( have_rows('files') ): ?>
					<ul class="list-dowloads">
						<?php while( have_rows('files') ): the_row();?>

						<!-- <li class="dowload"><?php echo $file ?></li> -->
						<?php get_template_part( 'components/normas/download-card' ); ?>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php get_template_part( 'components/footer' ); ?>
<script type="text/javascript">
</script>