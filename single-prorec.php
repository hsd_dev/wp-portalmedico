<?php if ( has_post_thumbnail()) : ?>
	<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
	<div class="thumb-large" style="background-image: url(<?php echo $thumbnail[0]; ?>)"></div>
	<?php else : ?>
	<div class="thumb-large no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
<?php endif; ?>

<?php
	$q = new WP_Query( array('post_type' => array( 'prorec' )));
	$editions = Array();

	class Edition{
		public $title;
		public $link;
		public $id;
	}

	if( $q->have_posts() ) {
		while( $q->have_posts() ) {
			$q->the_post();
			$edition = new Edition();
			$edition->title = get_field('numero_da_edicao');
			$edition->link = get_permalink();
			$edition->id = $post->ID;

			array_push($editions, $edition);
		}
	}
?>

<?php get_template_part( 'components/header' ); ?>
	<div class="container-page post-inner prorec-inner">
		<div class="header-inner">
			<button class="btn-back"></button>
			<div class="tab-list">
				<?php foreach ($editions as $key => $value): ?>
					<?php $current_class = ( $editions[$key] -> id == $post->ID ) ? 'active' : ''; ?>

					<div class="tab <?php if ( $current_class ) echo $current_class; ?>">
						<a href="<?php echo $editions[$key] -> link ?>"><?php echo $editions[$key] -> title ?> Edição</a>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<div class="post">
			<div class="header-post">
				<div class="top">
					<span class="type">Prorec</span>
					<h2 class="title"><?php the_title() ?></h2>
					<p class="description">Entenda sua rotina e desafios como médico</p>
				</div>
				<div class="bottom">
					<div class="edition">
						<div class="group">
							<span class="month">Nº</span>
							<span class="description">edição</span>
						</div>
						<h3 class="number">#<?php the_field('numero_da_edicao') ?></h3>
					</div>
					<span class="publication">
						<span class="description-date">Publicado há:</span>
						<time class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp')) ?></time>
					</span>
				</div>
			</div>

			<div class="content-post">
				<div class="header-content-post">
					<div class="group-name">
						<span class="description">conhecendo</span>
						<h3 class="title"><?php the_title() ?></h3>
					</div>
					<div class="row row-intro">
						<div class="row row-text">
							<?php the_content() ?>
						</div>

						<ul class="list-infos">
							<?php
								$especialidade = get_field('especialidade');
								$tempo_de_atividade = get_field('tempo_de_atividade');
								$minhas_atividades = get_field('minhas_atividades');
							?>
							<li class="info">
								<span class="text"><strong>Especialidade:</strong> <?php echo $especialidade ?> </span>
							</li>
							<li class="info">
								<span class="text"><strong>Tempo de atividade:</strong> <?php echo $tempo_de_atividade ?> anos</span>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="photo">
						<?php $foto_medicoemfoco_1 = get_field('foto_medicoemfoco_1') ?>
						<img src="<?php echo esc_url( $foto_medicoemfoco_1['url'] ) ?>" alt="">
					</div>
				</div>
				<?php if( have_rows('minhas_atividades') ): ?>

					<div class="row">

					<?php while( have_rows('minhas_atividades') ): the_row();
						$titulo_atividade = get_sub_field('titulo_atividade');
						$texto_atividade = get_sub_field('texto_atividade');
					?>

						<div class="column column-large">
							<div class="content-row">
								<span class="title-row"><?php echo $titulo_atividade ?></span>
								<div class="text"><?php echo $texto_atividade ?></div>
							</div>
						</div>

					<?php endwhile; ?>

					</div>

				<?php endif; ?>
				<div class="row row-galeria">
					<?php 
					$images = get_field('galeria');
					$size = 'large'; // (thumbnail, medium, large, full or custom size)
					if( $images ): ?>
						<?php foreach( $images as $image ): ?>
							<div class="photo">
								<img src="<?php echo esc_url($image['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="footer-post">
				<h3 class="title-bg">
					<div class="highlight">
						Somos um time repleto de pessoas fantasticas
					</div>
				</h3>
				<?php
					$redes_sociais = get_field('redes_sociais');
					$facebook = $redes_sociais['facebook'];
					$instagram = $redes_sociais['instagram'];
					$linkedin = $redes_sociais['linkedin'];
				?>
				<?php if ($facebook || $instagram || $linkedin): ?>
					<div class="follow">
						<span class="username">Siga o <strong class="name">@<?php echo the_title() ?></strong></span>
						<ul class="list-redes">
							<?php if ($instagram): ?>
								<li class="rede instagram">
									<a href="<?php echo $instagram; ?>">Instagram</a>
								</li>
							<?php endif ?>
							<?php if ($facebook): ?>
								<li class="rede facebook">
									<a href="<?php echo $facebook; ?>">Facebook</a>
								</li>
							<?php endif ?>
							<?php if ($linkedin): ?>
								<li class="rede linkedin">
									<a href="<?php echo $linkedin; ?>">Linkedin</a>
								</li>
							<?php endif ?>
						</ul>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
<?php get_template_part( 'components/footer' ); ?>