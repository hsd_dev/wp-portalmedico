<?php get_template_part( 'components/header' ); ?>
    <div class="section-page praticas">
        <div class="header-page">
            <div class="content-header">
                <h2 class="title"><?php echo get_the_title( $page->ID ) ?></h2>
                <p><?php echo get_post_field('post_content', $page->ID) ?></p>
            </div>
            <?php get_template_part( 'components/tabs-pages' ); ?>
        </div>
        <div class="content-page">
            <?php if ( has_post_thumbnail()) : ?>
                <?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
                <img class="thumb" src="<?php echo $thumbnail[0]; ?>"></img>
                <?php else : ?>
                <div class="thumb no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
            <?php endif; ?>

            <div class="content">
                <?php echo the_content() ?>
            </div>
        </div>
    </div>
<?php get_template_part( 'components/footer' ); ?>