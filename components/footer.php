		<footer class="footer">
			<div class="header-footer">
				<div class="logo"></div>
			</div>
			<ul class="list-locais">
				<li class="local">
					<span class="title">Hospital São Domingos</span>
					<span class="phone">3216-8100</span>
					<span class="address">Av. Jerônimo de Albuquerque, 540, Bequimão, São Luís/MA - CEP 65060-645</span>
				</li>
				<li class="local">
					<span class="title">Unidade Diagnóstica Shopping Passeio</span>
					<span class="phone">3216-8100</span>
					<span class="address">Avenida Contorno Norte, 145 - Cohatrac 4 - São Luís/MA - CEP 65054-375</span>
				</li>
				<li class="local">
					<span class="title">Unidade Diagnóstica Golden Shopping Calhau</span>
					<span class="phone">3216-8100</span>
					<span class="address">Av. Avicenia, 200 - Calhau, São Luís - MA, CEP 65071-800</span>
				</li>
				<li class="local">
					<span class="title">Unidade Diagnóstica Pátio Norte Shopping</span>
					<span class="phone">3216-8100</span>
					<span class="address">Estrada de São José de Ribamar, MA-201, Rua Saramanta, 1.000 - Km-05 - Bairro Saramanta, São José de Ribamar - MA, CEP 65110-000</span>
				</li>
			</ul>
			<nav class="menu-footer">
				<?php wp_nav_menu( array( 'theme_location' => 'principal', 'container_class' => 'list' ) )?>
				<?php wp_nav_menu( array( 'theme_location' => 'footer-links', 'container_class' => 'list links' ) )?>
			</nav>
		</footer>
	</main>
</div>

<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.2.0/zxcvbn.js"></script>

<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.mask.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/js.cookie.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/main-min.js"></script>
</body>
</html>