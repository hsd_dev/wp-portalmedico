<?php 
	$file = get_sub_field('documento');
	$title = get_sub_field('titulo');
	$descricao = get_sub_field('descricao');
	// $unixtimestamp = strtotime($file['modified']);
	$date = mysql2date( "d/m/Y", $file['modified'] );
?>
<li class="dowload">
	<div class="content-item">
		<div class="header-item">
			<span class="type"><?php echo $file['subtype'] ?></span>
			<span class="publication">Publicado: <?php echo $date ?></span>
		</div>
		<div class="content">
			<h3 class="title"><?php echo $title ?></h3>
			<p class="description"><?php echo $descricao ?></p>
		</div>
	</div>
	<div class="footer-item">
		<div class="size-content">
			<span class="size"><?php echo formatSizeUnits($file['filesize']) ?></span>
			<span class="description">Tamanho do download</span>
		</div>
		<div class="buttons">
			<a href="<?php echo $file['url'] ?>" download="<?php echo $title ?>" class="btn-dowload">Baixar</a>
		</div>
	</div>
</li>