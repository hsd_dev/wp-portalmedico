<?php
    $name_post_type = get_post_type_object( get_post_type() );
?>
<li class="banner swiper-slide">
	<a href="<?php echo get_permalink() ?>">
		<?php if ( has_post_thumbnail()) : ?>
			<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), true); ?>
			<div class="image", style="background-image:url(<?php echo $thumbnail[0]; ?>)"></div>
			<?php else : ?>
			<div class="image no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
		<?php endif; ?>
		<div class="content">
			<div class="categoria">
				<span class="description"><?php echo $name_post_type->labels->sub_title ?></span>
				<h2 class="title"><?php echo $name_post_type->labels->title ?></h2>
				<span>
				</span>
			</div>
			<div class="infos">
				<h3 class="title"><?php the_title() ?></h3>
				<div class="description"><?php echo the_excerpt_max_charlength(150) ?></div>
				<span class="publication">
					<span class="description-date">Publicado há:</span>
					<time class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp')) ?></time>
				</span>
			</div>
		</div>
	</a>
</li>