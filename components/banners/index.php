<?php
	$q = new WP_Query( array('post_type' => array('semjaleco','prorec', 'post'),'posts_per_page' => 4, 'paged'=>$paged, 'order' => 'DESC' ));
?>
<?php if ($q->have_posts()): ?>
	<div class="banners swiper-container">
		<ul class="list swiper-wrapper">
			<?php 
				while( $q->have_posts() ) {
					$q->the_post();
					get_template_part( 'components/banners/banner' );
				}
			?>
		</ul>
		<div class="swiper-pagination"></div>
	</div>
<?php endif ?>

<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script>
	$(function() {
		let mySwiper = new Swiper ('.swiper-container', {
			loop: true,
			centeredSlides: true,
			spaceBetween: 30,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			}
		})
	});
</script>