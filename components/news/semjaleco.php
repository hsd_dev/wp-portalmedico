<?php
	$q = new WP_Query( array('post_type' => array( 'semjaleco' ),'posts_per_page' => 999, 'paged'=>$paged, 'order' => 'DESC' ));

	$eventos_edicoes = Array();
	$edicoes = Array();

	if( $q->have_posts() ) {
		while( $q->have_posts() ) {
			$q->the_post();
			array_push($eventos_edicoes, $q->post);
			array_push($edicoes, get_field('numero_da_edicao'));
		}
	}
?>

<?php if ($q->have_posts()): ?>
	<section class="section-semjaleco">
		<div class="header-section">
			<h2 class="title-section">Sem Jaleco</h2>
			<div class="tab-list">
				<?php foreach ($edicoes as $key_edicao => $value) { ?>
					<div class="tab" data-edicao="<?php echo $edicoes[$key_edicao]?>">#<?php echo $edicoes[$key_edicao] ?> Edição</div>
				<?php } ?>
			</div>
		</div>
		<div class="tab-content">
			<ul class="list-edicoes large">
				<?php
					foreach ($edicoes as $key_edicao => $value) {
						$q->the_post();
						get_template_part( 'components/news/edicao-card' );
					}
				?>
			</ul>
		</div>
	</section>
<?php endif ?>

<script>
	$(function() {
		let currentEdicao = <?php echo json_encode($edicoes[0]) ?>
		
		function activeEdicao(edicao){
			$('.section-semjaleco .tab-list .tab').removeClass('active');
			$('.section-semjaleco .tab-list .tab[data-edicao='+edicao+']').addClass('active');

			$('.section-semjaleco .list-edicoes .edicao').removeClass('active');
			$('.section-semjaleco .list-edicoes .edicao[data-edicao='+edicao+']').addClass('active');
		}

		activeEdicao(currentEdicao);

		$('.section-semjaleco .tab').on('click', function(event) {
			event.preventDefault();
			let currentEdicao = $(this).attr('data-edicao');
			activeEdicao(currentEdicao);
		});
	});
</script>