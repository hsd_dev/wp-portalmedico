<li class="edicao" data-edicao="<?php echo get_field('numero_da_edicao')?>">
	<a href="<?php echo get_permalink() ?>">
		<?php if ( has_post_thumbnail()) : ?>
			<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
			<div class="image", style="background-image:url(<?php echo $thumbnail[0]; ?>)"></div>
			<?php else : ?>
			<div class="image no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
		<?php endif; ?>
		<div class="content-edicao">
			<div class="header-content">
				<div class="publication">
					<span class="description-date">Publicado há:</span>
					<time class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp')) ?></time>
				</div>
				<div class="infos">
					<h3 class="title"><?php the_title() ?></h3>
					<span class="number">Edição #<?php the_field('numero_da_edicao') ?></span>
				</div>
			</div>
			<div class="text">
				<p class="substract"><?php echo get_the_excerpt(); ?></p>
				<span class="link">Ler edição</span>
			</div>
		</div>
	</a>
</li>