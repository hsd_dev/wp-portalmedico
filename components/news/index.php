<?php $q = new WP_Query( array('post_type' => array( 'post' ),'posts_per_page' => 4, 'paged'=>$paged, 'order' => 'DESC' )); ?>

<?php if ($q->have_posts()): ?>
	<section class="section-news">
		<div class="header-section">
			<h2 class="title-section">Notícia</h2>
		</div>
		<ul class="list-news destaque">
			<?php 
				while( $q->have_posts() ) {
					$q->the_post();
					get_template_part( 'components/news/new-card' );
				}
			?>
		</ul>
	</section>
<?php endif ?>