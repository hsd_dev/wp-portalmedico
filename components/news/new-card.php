<li class="new">
	<a href="<?php echo get_permalink() ?>">
		<?php if ( has_post_thumbnail()) : ?>
			<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
			<div class="image", style="background-image:url(<?php echo $thumbnail[0]; ?>)"></div>
			<?php else : ?>
			<div class="image no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
		<?php endif; ?>
		<div class="content-new">
			<div class="header-content">
				<div class="publication">
					<span class="description-date">Publicado há:</span>
					<time class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp')) ?></time>
				</div>
			</div>
			<div class="text">
				<h3 class="title"><?php the_title() ?></h3>
				<p class="substract"><?php echo the_excerpt_max_charlength(180) ?></p>
			</div>
		</div>
	</a>
</li>