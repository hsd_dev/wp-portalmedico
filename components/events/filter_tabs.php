<?php
	class Month{
		public $title;
		public $posts = Array();
	}

	$q = new WP_Query( array('post_type' => array( 'educacaomedica' ),'posts_per_page' => 999, 'paged'=>$paged, 'order' => 'DESC' ));

	$eventos_arr = Array();
	$months = Array();
	$months_objs = Array();

	if( $q->have_posts() ) {
		while( $q->have_posts() ) {
			$q->the_post();
			array_push($eventos_arr, $q->post);
			$date_string = strtotime(get_field('data_de_inicio'));

			array_push($months, date_i18n( "F", $date_string ));
		}
	}

	$months_unique = array_unique($months);

	foreach ($months_unique as $key => $value) {
		$month = new Month();
		$month->title = $months_unique[$key];

		array_push($months_objs, $month);
	}

	foreach ($months_objs as $key_month => $month) {
		foreach ($eventos_arr as $key_evento => $evento) {
			$date_string_arr = strtotime(get_field('data_de_inicio', $eventos_arr[$key_evento]->ID));
			$current_month_evento = date_i18n( "F", $date_string_arr );

			if ($months_objs[$key_month] -> title == $current_month_evento) {
				array_push($months_objs[$key_month] -> posts, $q->post);
			}
		}
	}
?>