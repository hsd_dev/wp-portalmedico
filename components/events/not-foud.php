<div class="notfound-evento">
	<h3 class="title">Sem eventos para o mês de <strong>Outubro</strong></h3>
	<div class="icon"></div>
	<p class="description">m dicta voluptas neque in accusantium iusto odio, qui soluta? Earum nesciunt voluptatum sunt dolorem temporibus, voluptatibus aperiam.</p>
</div>

<script>
	$(function() {
		let currentMonthActive = $('.section-eventos .tab-list .tab.active').text();

		$('.notfound-evento strong').text(currentMonthActive)
	});
</script>