<?php 
	$unixtimestamp = strtotime( get_field('data_de_inicio') );
	$current_month = date_i18n( "F", $unixtimestamp);
	$currentDate = date_i18n( "d.M", $unixtimestamp);
?>
<li class="event" data-month="<?php echo get_field('data_de_inicio'); ?>">
	<a href="<?php echo get_permalink() ?>">
		<?php if ( has_post_thumbnail()) : ?>
			<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
			<div class="image", style="background-image:url(<?php echo $thumbnail[0]; ?>)"></div>
			<?php else : ?>
			<div class="image no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
		<?php endif; ?>
		<div class="content-evento">
			<div class="header-content">
				<span class="categoria">Encontro</span>
				<div class="wrap-date">
					<h3 class="date"><?php echo $currentDate  ?></h3>
					<?php $horarios = get_field('horarios')  ?>
					<?php if( $horarios ): ?>
						<h3 class="hours"><?php echo esc_attr( $horarios['inicio'] ) ?></h3>
					<?php endif; ?>
				</div>
			</div>
			<div class="text">
				<h3 class="title"><?php the_title() ?></h3>
				<p class="substract"><?php echo the_excerpt_max_charlength(140) ?></p>
			</div>
			<span class="local"><?php echo the_field('local'); ?></span>
		</div>
	</a>
</li>