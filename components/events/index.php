<?php
	//Pegando o caminho relativo à esse arquivo.
	include get_template_directory().'/components/events/filter_tabs.php';
?>
<?php if ($q->have_posts()): ?>
	<section class="section-eventos">
		<div class="header-section">
			<h2 class="title-section">Educação Médica</h2>
			<div class="tab-list">
				<?php foreach ($months_objs as $key_month => $value) { ?>
					<div class="tab" data-month="<?php echo $months_objs[$key_month] -> title ?>"><?php echo ucfirst($months_objs[$key_month] -> title) ?></div>
				<?php } ?>
			</div>
		</div>
		<?php foreach ($months_objs as $key_month => $value): ?>
			<div class="tab-content" data-month="<?php echo $months_objs[$key_month] -> title ?>">
				<div class="list-events">
					<?php
						foreach ($months_objs[$key_month] -> posts as $key => $value) {
							$q->the_post();
							get_template_part( 'components/events/event-card' );
						}
					?>
				</div>
			</div>
		<?php endforeach ?>
	</section>
<?php endif ?>

<script>
	$(function() {
		let currentMonth = <?php echo json_encode($months_objs[0] -> title) ?>
		
		function activeMonth(month){
			$('.section-eventos .tab-content').removeClass('active');
			$('.section-eventos .tab-list .tab').removeClass('active');
			$('.section-eventos .tab-content[data-month='+month+']').addClass('active');
			$('.section-eventos .tab-list .tab[data-month='+month+']').addClass('active');
		}

		activeMonth(currentMonth);

		$('.section-eventos .tab').on('click', function(event) {
			event.preventDefault();
			let currentMonth = $(this).attr('data-month');
			activeMonth(currentMonth);
		});
	});
</script>