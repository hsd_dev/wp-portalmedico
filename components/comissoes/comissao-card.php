<div class="comissao">
	<div class="header-comissao">
		<h3 class="title"><?php the_title() ?></h3>
		<div class="infos">
			<span class="phone"><?php the_field('telefone') ?></span>
			<span class="hours"><?php the_field('horario') ?></span>
		</div>
	</div>
	<div class="members"><span class="title">membros</span>
		<?php $presidente = get_field('presidente'); ?>

		<div class="content-members" <?php if($presidente) echo 'data-presidente="true"' ?>>
			<?php 
				if( $presidente ): ?>
					<ul class="presidente">
						<?php foreach( $presidente as $p ): // variable must NOT be called $post (IMPORTANT) ?>
							<li class="member">
								<div class="content-infos">
									<?php if ( has_post_thumbnail($p->ID)) : ?>
										<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($p->ID), 'large'); ?>
										<div class="photo", style="background-image:url(<?php echo $thumbnail[0]; ?>)"></div>
										<?php else : ?>
										<div class="photo no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
									<?php endif; ?>
									<span class="cargo">Presidente</span>
									<span class="name"><?php echo get_the_title( $p->ID ); ?></span>
								</div>
								<div class="footer-infos">
									<div class="infos">
										<?php 

										$posts = get_field('especialidade', $p->ID);

										if( $posts ): ?>
											<?php foreach( $posts as $p ):?>
												<span class="especialidade"><?php echo get_the_title( $p->ID ); ?></span>
											<?php endforeach; ?>
										<?php endif; ?>
										<span class="crm">crm: <?php the_field('crm', $p->ID); ?></span>
									</div>
									<button class="go"></button>
								</div>
							</li>
						<?php endforeach; ?>
					</ul>

			<?php endif; ?>
			<?php 
				$posts = get_field('membros');

				if( $posts ): ?>
					<ul class="list-members">
						<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
							<li class="member">
								<div class="content-infos">
									<?php if ( has_post_thumbnail($p->ID)) : ?>
										<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($p->ID), 'large'); ?>
										<div class="photo", style="background-image:url(<?php echo $thumbnail[0]; ?>)"></div>
										<?php else : ?>
										<div class="photo no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
									<?php endif; ?>
									<span class="name"><?php echo get_the_title( $p->ID ); ?></span>
								</div>
								<div class="footer-infos">
									<div class="infos">
										
										<?php 

										$posts = get_field('especialidade', $p->ID);

										if( $posts ): ?>
											<?php foreach( $posts as $p ):?>
												<span class="especialidade"><?php echo get_the_title( $p->ID ); ?></span>
											<?php endforeach; ?>
										<?php endif; ?>
										
										<span class="crm">crm: <?php the_field('crm', $p->ID); ?></span>
									</div>
									<button class="go"></button>
								</div>
							</li>
						<?php endforeach; ?>
					</ul>

			<?php endif; ?>
		</div>
	</div>
</div>