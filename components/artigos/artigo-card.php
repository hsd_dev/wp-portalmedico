<li class="artigo">
	<?php 
		$autor = get_field('autor');
	?>

	<?php
		if( $autor ): ?>
			<?php foreach( $autor as $p ): // variable must NOT be called $post (IMPORTANT) ?>
				<?php if ( has_post_thumbnail($p->ID)) : ?>
					<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($p->ID), 'large'); ?>
					<div class="photo", style="background-image:url(<?php echo $thumbnail[0]; ?>)"></div>
					<?php else : ?>
					<div class="photo no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
				<?php endif; ?>
				<div class="infos">
					<div class="header-infos">
						<span class="name"><?php echo get_the_title( $p->ID ); ?></span>

						<?php 
							$especialidade = get_field('especialidade');

							if( $especialidade ): ?>
								<?php foreach( $especialidade as $p ):?>
									<span class="especialidade"><?php echo get_the_title( $p->ID ); ?></span>
								<?php endforeach; ?>
						<?php endif; ?>

					</div>
					<div class="wrap-infos">
						<h3 class="title"><?php the_title(); ?></h3>
						<span class="description"><?php the_content(); ?></span>
					</div>
					<div class="footer-infos">
						<?php
						$documento = get_field('documento');
						if( $documento ): ?>
							<div class="type">
								<span class="title-type"><?php echo $documento['subtype'] ?></span>
								<span class="date">Publicado há: <?php echo human_time_diff( get_the_time('U'), current_time('timestamp')) ?></span>
							</div>
							<div class="group-buttons">
						   		<a class="btn-view" target="__blank" href="<?php echo $documento['url']; ?>">Visualizar</a>
						   		<a class="btn-download" download="<?php echo $documento['name']; ?>" href="<?php echo $documento['url']; ?>">Baixar</a>
							</div>
						<?php endif; ?>

					</div>
				</div>
			<?php endforeach; ?>
	<?php endif; ?>
</li>