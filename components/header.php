<!DOCTYPE html>
<html <?php language_attributes(); ?> <?php body_class(); ?>>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154847290-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-154847290-1');
	</script>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<?php wp_head(); ?>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/styles/dist/style.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/auth-min.js"></script>
	<!-- <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script> -->
	<!-- <script src="<?php echo get_template_directory_uri() ?>/assets/js/main-min.js"></script> -->
</head>

<body <?php body_class($_COOKIE['user-token'] ? 'auth' : ''); ?>>
	<div id="app">
		<?php get_template_part( 'components/formulario-medico/pages/form' ); ?>
		<main class="main">
			<?php $bg = get_query_var('bg'); ?>
			
			<?php if ($bg): ?>
				<div class="thumb-large" style="background-image: url(<?php echo $bg ?>)"></div>
			<?php else: ?>
				<div class="thumb-large no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
			<?php endif ?>

			<nav class="main-menu">
				<div class="wrap-menu">
					<div class="group-logo">
						<a href="<?php echo get_home_url(); ?>" class="wrap-logo">
						</a>

						<button class="btn-close">Fechar</button>
						<button class="btn-search">Buscar</button>
					</div>

					<div class="group-top">
						<div class="bem-vindo">
							<?php 
								$unixtimestamp = strtotime(date('y/m/d'));
								$currentDate = date_i18n( "l, d - M", $unixtimestamp);
							?>
							<span class="date"><?php echo $currentDate ?></span>
							<h3 class="title">Seja bem-vindo</h3>
						</div>
						
						<?php
							$post_data = get_post($post->post_parent);
							$parent_slug = $post_data->post_name;
						?>

						<?php if($parent_slug === 'auth') : ?>
						    <div class="menu-auth">
								<?php wp_nav_menu( array( 'theme_location' => 'auth-menu', 'container_class' => 'list' ) )?>
							</div>
						<?php else : ?>
						    <div class="menu-portal">
								<?php wp_nav_menu( array( 'theme_location' => 'principal', 'container_class' => 'list' ) )?>
							</div>
						<?php endif; ?>
						<!-- parent-pageid-10 logged-in auth -->
					</div>

					<div class="group-footer">
						<div class="tools-menu-mobile">
							<div class="wrap-login">
								<div class="header-login">
									<div class="icon"></div>
									<div class="infos">
										<span class="title">Acesse sua conta</span>
										<span class="description">Faça login para você ver sua agenda do Hospital São Domingos.</span>
									</div>
								</div>
								<button class="btn-login">Entrar</button>
								<button class="btn-logout">Sair</button>
							</div>
							<?php wp_nav_menu( array( 'theme_location' => 'tools', 'container_class' => 'list' ) )?>
							<div class="wrap-cadastro">
								<div class="ilustration"></div>
								<div class="infos-form">
									<span class="novidade">New</span>
									<span class="title">Novo Cadastro médico</span>
									<span class="description">Venha para a familia HSD</span>
									<button class="btn-cadastro disable">Em breve</button>
									<!-- <button class="btn-cadastro">Quero me cadastrar</button> -->
								</div>
							</div>
						</div>

						<div class="langs">
							<ul class="list">
								<li class="lang active">Pt</li>
							</ul>
						</div>
						<div class="social">
							<ul class="list">
								<li class="item instagram"><a href="https://www.instagram.com/hospitalsaodomingos/">Instagram</a></li>
								<li class="item facebook"><a href="https://pt-br.facebook.com/saodomingos/">Facebook</a></li>
								<li class="item youtube"><a href="https://www.youtube.com/user/HospitalSaoDomingos">Youtube</a></li>
							</ul>
						</div>
					</div>
				</div>
			</nav>

			<div class="form-login">
				<div class="overlay"></div>
				<div id="form-login" class="container-form" data-step="username">
					<div class="button-close"></div>
					<div class="header-form">
						<h3 class="title">Acesso a área privada</h3>
						<span class="description">Área destinada apenas para profissionais do hospital São Domingos</span>
					</div>
	
					<form class="content-form step-username">
						<div class="wrap-inputs">
							<div class="wrap-input">
								<input type="text" name="username" class="input-cpf">
								<div class="loading">
									<div class="dot"></div>
					            </div>
								<label class="placeholder">
									<span class="text">Digite seu CPF</span>
								</label>
							</div>
							<span class="erro erro-username">qweqweq</span>
						</div>
						<button class="btn-signin btn-proximo">Próximo</button>
					</form>

					<form class="content-form step-password">
						<div class="header-user">
							<div class="icon"></div>
							<div class="infos">
								<span class="description">Bem vindo</span>
								<h3 class="title">CPF</h3>
							</div>
						</div>
						<div class="wrap-inputs">
							<div class="wrap-input">
								<input type="password" name="password" class="input-password">
								<div class="loading">
									<div class="dot"></div>
					            </div>
								<label class="placeholder">
									<span class="text">Digite sua senha</span>
								</label>
							</div>
							<span class="erro erro-password"></span>
						</div>
						<button class="btn-signin btn-entrar">Fazer login</button>
						<a class="btn-backcpf">Tentar outro CPF</a>
					</form>

					<form class="content-form step-firstlogin">
						<div class="header-user">
							<div class="icon"></div>
							<div class="infos">
								<span class="description">Olá, Dr.</span>
								<h3 class="title">Crie sua senha.</h3>
							</div>
						</div>
						<div class="wrap-inputs">
							<div class="wrap-input">
								<input type="password" name="password" class="input-newPassword">
								<div class="loading">
									<div class="dot"></div>
					            </div>
								<label class="placeholder">
									<span class="text">Nova senha</span>
								</label>
							</div>

							<div class="wrap-strength">
								<p class="password-strength-text"></p>
								<div class="password-strength-meter"></div>
							</div>
		
							<ul class="tips">
								<li class="tip">Use no mínimo 6 caracteres</li>					
								<li class="tip">Além das letras, inclua pelo menos um número ou símbolo</li>	
							</ul>
							<span class="erro erro-password"></span>

						</div>
						<button class="btn-signin btn-new">Redefinir</button>
						<a class="btn-backcpf">Tentar outro CPF</a>
					</form>
					
					<span class="logo">HSD</span>
				</div>
			</div>

			<div class="app-group">
				<div class="aside-apps">
					<div class="wrap-apps">
						<button class="open-apps dot"></button>
						<div class="wrap-options">
							<div class="options">
								<div class="wrap-login">
									<div class="header-login">
										<div class="icon"></div>
										<div class="infos">
											<span class="title">Login</span>
											<span class="description">Faça login para você ver sua agenda do Hospital São Domingos.</span>
										</div>
									</div>
									<button class="btn-login">Fazer login</button>
								</div>
								<?php wp_nav_menu( array( 'theme_location' => 'tools', 'container_class' => 'list' ) )?>
								<div class="wrap-cadastro">
									<div class="ilustration"></div>
									<div class="infos-form">
										<span class="novidade">New</span>
										<span class="title">Novo Cadastro médico</span>
										<span class="description">Venha para a familia HSD</span>
										<button class="btn-cadastro disable">Em breve</button>
										<!-- <button class="btn-cadastro">Quero me cadastrar</button> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="inner">
					<header class="header">
						<div class="header-mobile">
							<div class="button-mobile">
								<div class="line"></div>
								<div class="line"></div>
								<div class="line"></div>
							</div>
							<a href="<?php echo get_home_url(); ?>" class="wrap-logo">
							</a>
						</div>
						<div class="header-auth">
							<?php wp_nav_menu( array( 'theme_location' => 'auth-links', 'container_class' => 'list' ) )?>
							<div class="wrap-profile">
								<div class="photo">
									<div class="icon"></div>
								</div>
								<div class="group">
									<div class="group-auth">
										<div class="icon"></div>
										<div class="infos">
											<span class="title">Vital Lima</span>
											<span class="description">CRM</span>
										</div>
									</div>
									<button class="btn-logout">Sair</button>
								</div>
							</div>
						</div>
						<form class="search" role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
							<input class="input-search" type="text" name="s" placeholder="Buscar"/>
							<button class="btn-search" type="submit" alt="Buscar">Buscar</button>
						</form>
					</header>

					<div class="main-container">
