<?php
	$page = get_page_by_title( 'Home' );
    $dowloands_sections = get_field('dowloands_sections', $page->ID);
?>

<?php if ($dowloands_sections): ?>
	<div class="section-downloads">
		<?php foreach( $dowloands_sections as $download): ?>
			<?php setup_postdata($download); ?>
			<div class="group-download">
				<div class="header-downloads">
					<h3 class="title"><?php echo get_the_title( $download->ID ); ?></h3>
				</div>
				<?php if( have_rows('files', $download->ID) ): ?>
					<ul class="list">
						<?php 
							$counter = 0;
							$max = 3;
						?>
						<?php while( have_rows('files', $download->ID) and ($counter < $max)): the_row();
							$file = get_sub_field('documento');
							$title = get_sub_field('titulo');
							$descricao = get_sub_field('descricao');
							$counter++;
						?>
						<li class="download">
							<a href="<?php echo $file['url'] ?>" download="<?php echo $title ?>" class="wrap-download">
								<div class="content">
									<span class="title"><?php echo $title ?></span>
									<p class="description"><?php echo mb_strimwidth($descricao, 0, 45, '...'); ?></p>
								</div>
								<a href="<?php echo $file['url'] ?>" download="<?php echo $title ?>" class="btn-download">Baixar</a>
							</a>
						</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	</div>
<?php endif ?>