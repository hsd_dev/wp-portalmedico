<?php
// Date formate Top
function wordpressify_resources() {
	wp_enqueue_style('style', get_stylesheet_uri());
}

add_action('wp_enqueue_scripts', 'wordpressify_resources');

// Customize excerpt word count length
function custom_excerpt_length() {
	return 22;
}

add_filter('excerpt_length', 'custom_excerpt_length');

function the_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '[...]';
	} else {
		echo $excerpt;
	}
}

add_action('rest_api_init', 'register_rest_images' );
function register_rest_images(){
    register_rest_field( array('post'),
        'fimg_url',
        array(
            'get_callback'    => 'get_rest_featured_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
function get_rest_featured_image( $object, $field_name, $request ) {
    if( $object['featured_media'] ){
        $img = wp_get_attachment_image_src( $object['featured_media'], 'app-thumb' );
        return $img[0];
    }
    return false;
}

add_theme_support( 'custom-logo' );

add_theme_support( 'custom-logo', array(
	'height'      => 100,
	'width'       => 400,
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
) );

function my_login_logo_one() { 
	?>
		<style type="text/css"> 
			body.login div#login h1 a {
				background-image: url(<?php echo get_template_directory_uri() ?>/images/logo.svg);  //Add your own logo image in this url 
				padding-bottom: 30px;
				width: 100%;
				background-size: contain;
				pointer-events: none;
			}
		</style>
	<?php 
}

add_action( 'login_enqueue_scripts', 'my_login_logo_one' );

function limit($value, $limit = 100, $end = '...')
{
	if (strlen($value) > $limit)
	   return $value = substr($value, 0, $limit) . $end;
}

function ws_register_images_field() {
    register_rest_field( 
        'post',
        'images',
        array(
            'get_callback'    => 'ws_get_images_urls',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

add_action( 'rest_api_init', 'ws_register_images_field' );

function ws_get_images_urls( $object, $field_name, $request ) {
    $medium = wp_get_attachment_image_src( get_post_thumbnail_id( $object->id ), 'medium' );
    $medium_url = $medium['0'];

    $large = wp_get_attachment_image_src( get_post_thumbnail_id( $object->id ), 'large' );
    $large_url = $large['0'];

    return array(
        'medium' => $medium_url,
        'large'  => $large_url,
    );
}

// Theme setup
function wordpressify_setup() {
	// Handle Titles
	add_theme_support( 'title-tag' );

	// Add featured image support
	add_theme_support('post-thumbnails');
	add_image_size('small-thumbnail', 720, 720, true);
	add_image_size('square-thumbnail', 80, 80, true);
	add_image_size('banner-image', 1024, 1024, true);
}

add_action('after_setup_theme', 'wordpressify_setup');

show_admin_bar(false);

// Checks if there are any posts in the results
function is_search_has_results() {
	return 0 != $GLOBALS['wp_query']->found_posts;
}

// Add Widget Areas
function wordpressify_widgets() {
	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'sidebar1',
		'before_widget' => '<div class="widget-item">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	));
}

function wpb_custom_new_menu() {
	register_nav_menu('principal',__( 'Principal' ));
	register_nav_menu('footer-links',__( 'Footer Links' ));
	register_nav_menu('auth-links',__( 'Auth Links' ));
	register_nav_menu('auth-menu',__( 'Auth Menu' ));
	register_nav_menu('tools',__( 'Ferramentas' ));
}

add_action( 'init', 'wpb_custom_new_menu' );

add_action('widgets_init', 'wordpressify_widgets');

// Custom Pages
register_post_type('semjaleco', array(
	'labels' => array(
				'name' => __( 'Sem Jaleco' ),
				'singular_name' => __( 'Edição' ),
				'add_new' => __('Nova Edição'),
				'add_new_item' => __('Adicionar Nova Edição'),
				'edit_item' => __('Editar Edição'),
				'new_item' => __('Nova Edição'),
				'view_item' => __('Visualizar Edições'),
				'search_items' => __('Pesquisar Edições'),
				'sub_title' => __( 'Sem' ),
				'title' => __( 'Jaleco' )
	),
	'menu_position' => 6,
	'public' => true,
	'menu_icon' => 'dashicons-admin-comments',
	'has_archive' => false,
	'rewrite' => array('slug' => 'semjaleco'),
	'show_ui' => true,
	'show_in_rest' => true,
	'capability_type' => 'page',
	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor')
	)
);

// Custom Pages
register_post_type('Artigos', array(
	'labels' => array(
				'name' => __( 'Artigos' ),
				'singular_name' => __( 'Artigo' ),
				'add_new' => __('Nova Artigo'),
				'add_new_item' => __('Adicionar Nova Artigo'),
				'edit_item' => __('Editar Artigo'),
				'new_item' => __('Nova Artigo'),
				'view_item' => __('Visualizar Artigos'),
				'search_items' => __('Pesquisar Artigos'),
				'sub_title' => __( 'Artigos' ),
				'title' => __( 'Cientificos' )
	),
	'menu_position' => 6,
	'public' => true,
	'menu_icon' => 'dashicons-book-alt',
	'has_archive' => false,
	'rewrite' => array('slug' => 'artigos'),
	'show_ui' => true,
	'show_in_rest' => true,
	'capability_type' => 'page',
	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor')
	)
);

register_post_type('Especialidades', array(
	'labels' => array(
				'name' => __( 'Especialidades' ),
				'singular_name' => __( 'Especialidade' ),
				'add_new' => __('Nova Especialidade'),
				'add_new_item' => __('Adicionar Nova Especialidade'),
				'edit_item' => __('Editar Especialidade'),
				'new_item' => __('Nova Especialidade'),
				'view_item' => __('Visualizar Especialidades'),
				'search_items' => __('Pesquisar Especialidades'),
				'sub_title' => __( 'Especialidades' ),
				'title' => __( 'Médicas' )
	),
	'menu_position' => 6,
	'public' => true,
	'menu_icon' => 'dashicons-nametag',
	'has_archive' => false,
	'rewrite' => array('slug' => 'especialidades'),
	'show_ui' => true,
	'show_in_rest' => true,
	'capability_type' => 'page',
	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor')
	)
);

// register_post_type('iep', array(
// 	'labels' => array(
// 				'name' => __( 'IEP' ),
// 				'singular_name' => __( 'Pagina' ),
// 				'add_new' => __('Novo Pagina'),
// 				'add_new_item' => __('Adicionar Nova Pagina'),
// 				'edit_item' => __('Editar Pagina'),
// 				'new_item' => __('Nova Pagina'),
// 				'view_item' => __('Visualizar Paginas'),
// 				'search_items' => __('Pesquisar Paginas'),
// 				'sub_title' => __( 'IEP' ),
// 				'title' => __( 'IEP' )
// 	),
// 	'menu_position' => 6,
// 	'public' => true,
// 	'menu_icon' => 'dashicons-admin-comments',
// 	'has_archive' => false,
// 	'rewrite' => array('slug' => 'iep'),
// 	'show_ui' => true,
// 	'show_in_rest' => true,
// 	'capability_type' => 'page',
// 	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor')
// 	)
// );

register_post_type('prorec', array(
	'labels' => array(
				'name' => __( 'prorec' ),
				'singular_name' => __( 'Edição' ),
				'add_new' => __('Nova Edição'),
				'add_new_item' => __('Adicionar Nova Edição'),
				'edit_item' => __('Editar Edição'),
				'new_item' => __('Nova Edição'),
				'view_item' => __('Visualizar Edições'),
				'search_items' => __('Pesquisar Edições'),
				'sub_title' => __( 'Destaque' ),
				'title' => __( 'Prorec' )
	),
	'menu_position' => 6,
	'public' => true,
	'menu_icon' => 'dashicons-groups',
	'has_archive' => false,
	'rewrite' => array('slug' => 'prorec'),
	'show_ui' => true,
	'show_in_rest' => true,
	'capability_type' => 'page',
	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor')
	)
);

register_post_type('educacaomedica', array(
	'labels' => array(
				'name' => __( 'Educação Médica' ),
				'singular_name' => __( 'educacaomedica' ),
				'add_new' => __('Novo Evento'),
				'add_new_item' => __('Adicionar Novo Evento'),
				'edit_item' => __('Editar Evento'),
				'new_item' => __('Novo Evento'),
				'view_item' => __('Visualizar Eventos'),
				'search_items' => __('Pesquisar Eventos'),
				'sub_title' => __( 'Educação' ),
				'title' => __( 'médica' )
	),
	'menu_position' => 6,
	'public' => true,
	'menu_icon' => 'dashicons-calendar-alt',
	'has_archive' => false,
	'rewrite' => array('slug' => 'educacaomedica'),
	'show_ui' => true,
	'show_in_rest' => true,
	'capability_type' => 'page',
	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor')
	)
);

register_post_type('normas', array(
	'labels' => array(
				'name' => __( 'Normas e Diretrizes' ),
				'singular_name' => __( 'Norma' ),
				'add_new' => __('Nova Norma'),
				'add_new_item' => __('Nova Norma'),
				'edit_item' => __('Edita Norma/Diretriz'),
				'new_item' => __('Nova Norma'),
				'view_item' => __('Visualizar Normas/Diretrizes'),
				'search_items' => __('Pesquisar Normas/Diretrizes')
	),
	'hierarchical' => true,
	'public' => true,
	'rewrite' => array(
		'slug' => 'normas'
	),
	'menu_position' => 6,
	'public' => true,
	'menu_icon' => 'dashicons-download',
	'has_archive' => false,
	'rewrite' => array('slug' => 'normas'),
	'show_ui' => true,
	'show_in_rest' => true,
	'capability_type' => 'page',
	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor', 'page-attributes', 'suport')
	)
);

function formatSizeUnits($bytes){
	if ($bytes >= 1073741824)
	{
		$bytes = number_format($bytes / 1073741824, 2) . ' GB';
	}
	elseif ($bytes >= 1048576)
	{
		$bytes = number_format($bytes / 1048576, 2) . ' MB';
	}
	elseif ($bytes >= 1024)
	{
		$bytes = number_format($bytes / 1024, 2) . ' KB';
	}
	elseif ($bytes > 1)
	{
		$bytes = $bytes . ' bytes';
	}
	elseif ($bytes == 1)
	{
		$bytes = $bytes . ' byte';
	}
	else
	{
		$bytes = '0 bytes';
	}

	return $bytes;
}

register_post_type('comissoes', array(
	'labels' => array(
				'name' => __( 'Comissões Médicas' ),
				'singular_name' => __( 'Comissão' ),
				'add_new' => __('Nova Comissão'),
				'add_new_item' => __('Nova Comissão'),
				'edit_item' => __('Edita Comissão/Diretriz'),
				'new_item' => __('Nova Comissão'),
				'view_item' => __('Visualizar Comissões'),
				'search_items' => __('Pesquisar Comissões')
	),
	'hierarchical' => true,
	'public' => true,
	'rewrite' => array(
		'slug' => 'normas'
	),
	'menu_position' => 6,
	'public' => true,
	'menu_icon' => 'dashicons-media-document',
	'has_archive' => false,
	'rewrite' => array('slug' => 'comissoes'),
	'show_ui' => true,
	'show_in_rest' => true,
	'capability_type' => 'page',
	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor', 'page-attributes', 'suport')
	)
);

register_post_type('diretorias', array(
	'labels' => array(
				'name' => __( 'Diretorias Médicas' ),
				'singular_name' => __( 'Diretorias' ),
				'add_new' => __('Nova Diretoria'),
				'add_new_item' => __('Nova Diretoria'),
				'edit_item' => __('Edita Diretoria'),
				'new_item' => __('Nova Diretoria'),
				'view_item' => __('Visualizar Diretorias'),
				'search_items' => __('Pesquisar Diretorias')
	),
	'hierarchical' => true,
	'public' => true,
	'rewrite' => array(
		'slug' => 'diretorias'
	),
	'menu_position' => 6,
	'public' => true,
	'menu_icon' => 'dashicons-businessperson',
	'has_archive' => false,
	'rewrite' => array('slug' => 'diretorias'),
	'show_ui' => true,
	'show_in_rest' => true,
	'capability_type' => 'page',
	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor', 'page-attributes', 'suport')
	)
);

register_post_type('pessoas', array(
	'labels' => array(
				'name' => __( 'Pessoas' ),
				'singular_name' => __( 'Pessoa' ),
				'add_new' => __('Nova Pessoa'),
				'add_new_item' => __('Nova Pessoa'),
				'edit_item' => __('Edita Pessoa'),
				'new_item' => __('Nova Pessoa'),
				'view_item' => __('Visualizar Pessoas'),
				'search_items' => __('Pesquisar Pessoas')
	),
	'hierarchical' => true,
	'public' => true,
	'rewrite' => array(
		'slug' => 'pessoas'
	),
	'taxonomies'  => array( 'category' ),
	'menu_position' => 4,
	'public' => true,
	'menu_icon' => 'dashicons-admin-post',
	'has_archive' => false,
	'rewrite' => array('slug' => 'new'),
	'show_ui' => true,
	'show_in_rest' => true,
	'capability_type' => 'page',
	'supports' => array('title', 'thumbnail', 'revisions', 'excerpt', 'editor', 'page-attributes', 'suport')
	)
);

// add hook
add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_sub_menu', 10, 2 );
// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
  if ( isset( $args->sub_menu ) ) {
	$root_id = 0;
	
	// find the current menu item
	foreach ( $sorted_menu_items as $menu_item ) {
	  if ( $menu_item->current ) {
		// set the root id based on whether the current menu item has a parent or not
		$root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
		break;
	  }
	}
	
	// find the top level parent
	if ( ! isset( $args->direct_parent ) ) {
	  $prev_root_id = $root_id;
	  while ( $prev_root_id != 0 ) {
		foreach ( $sorted_menu_items as $menu_item ) {
		  if ( $menu_item->ID == $prev_root_id ) {
			$prev_root_id = $menu_item->menu_item_parent;
			// don't set the root_id to 0 if we've reached the top of the menu
			if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
			break;
		  } 
		}
	  }
	}
	$menu_item_parents = array();
	foreach ( $sorted_menu_items as $key => $item ) {
	  // init menu_item_parents
	  if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;
	  if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
		// part of sub-tree: keep!
		$menu_item_parents[] = $item->ID;
	  } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
		// not part of sub-tree: away with it!
		unset( $sorted_menu_items[$key] );
	  }
	}
	
	return $sorted_menu_items;
  } else {
	return $sorted_menu_items;
  }
}

function redirect($url, $permanent = false) {
	if (headers_sent() === false){
		header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
	}
	exit();
}

add_filter('single_template', function($original){
	global $post;
	$post_name = $post->post_name;
	$post_type = $post->post_type;
	$base_name = 'single-' . $post_type . '-' . $post_name . '.php';
	$template = locate_template($base_name);
	if ($template && ! empty($template)) return $template;
	return $original;
});

add_filter( 'kdmfi_featured_images', function( $featured_images ) {
	$args = array(
		'id' => 'featured-image-2',
		'label_name' => 'Featured Image 2',
		'label_set' => 'Set featured image 2',
		'label_remove' => 'Remove featured image 2',
		'label_use' => 'Set featured image 2',
		'post_type' => array( 'equipes', 'projetos', 'funcionarios', 'lideres', 'coringas'),
	);

	$featured_images[] = $args;

	return $featured_images;
});

function post_remove (){ 
   remove_menu_page('edit.php');
} 

// add_action('admin_menu', 'post_remove');

function wp_get_menu_array($current_menu) {
	$array_menu = wp_get_nav_menu_items($current_menu);
	$menu = array();
	foreach ($array_menu as $m) {
		if (empty($m->menu_item_parent)) {
			$menu[$m->ID] = array();
			$menu[$m->ID]['ID'] = $m->ID;
			$menu[$m->ID]['title'] = $m->title;
			$menu[$m->ID]['url'] = $m->url;
			$menu[$m->ID]['children'] = array();
		}
	}
	$submenu = array();
	foreach ($array_menu as $m) {
		if ($m->menu_item_parent) {
			$submenu[$m->ID] = array();
			$submenu[$m->ID]['ID'] = $m->ID;
			$submenu[$m->ID]['title'] = $m->title;
			$submenu[$m->ID]['url'] = $m->url;
			$menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
		}
	}
	return $menu;
}

function get_ajax_eventos() {
	$date = esc_attr( $_POST['date'] );
	// Query Arguments
	$args = array(
		'post_type' => array('educacaomedica'),
		'post_status' => array('publish'),
		'posts_per_page' => 4,
		'nopaging' => true,
		'order' => 'DESC',
		'monthnum' => $date
		// 'orderby' => 'date'
	);

	// The Query
	$ajaxposts = new WP_Query( $args );

	$response = '';

	// The Query
	if ( $ajaxposts->have_posts() ) {
		while ( $ajaxposts->have_posts() ) {
			$ajaxposts->the_post();
			$response .= get_template_part('components/events/event-card');
		}
	} else {
		$response .= get_template_part('components/events/not-foud');
	}

	echo $response;

	exit; // leave ajax call
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_ajax_posts', 'get_ajax_eventos');
add_action('wp_ajax_nopriv_get_ajax_posts', 'get_ajax_eventos');

function revcon_change_post_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Notícias';
	$submenu['edit.php'][5][0] = 'Notícias';
	$submenu['edit.php'][10][0] = 'Adicionar notícia';
	$submenu['edit.php'][16][0] = 'Notícias Tags';
	echo '';
}
function revcon_change_post_object() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Notícias';
	$labels->singular_name = 'Notícias';
	$labels->add_new = 'Adicionar notícia';
	$labels->add_new_item = 'Adicionar notícia';
	$labels->edit_item = 'Editar notícia';
	$labels->new_item = 'Notícias';
	$labels->view_item = 'Visualizar notícia';
	$labels->search_items = 'Buscar notícia';
	$labels->not_found = 'No Post found';
	$labels->not_found_in_trash = 'No Post found in Trash';
	$labels->all_items = 'All Posts';
	$labels->menu_name = 'Notícias';
	$labels->name_admin_bar = 'Notícias';
	$labels->sub_title = 'Notícia';
	$labels->title = 'Destaque';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

function bidirectional_acf_update_value( $value, $post_id, $field  ) {
	
	// vars
	$field_name = $field['name'];
	$field_key = $field['key'];
	$global_name = 'is_updating_' . $field_name;
	
	
	// bail early if this filter was triggered from the update_field() function called within the loop below
	// - this prevents an inifinte loop
	if( !empty($GLOBALS[ $global_name ]) ) return $value;
	
	
	// set global variable to avoid inifite loop
	// - could also remove_filter() then add_filter() again, but this is simpler
	$GLOBALS[ $global_name ] = 1;
	
	
	// loop over selected posts and add this $post_id
	if( is_array($value) ) {
	
		foreach( $value as $post_id2 ) {
			
			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);
			
			
			// allow for selected posts to not contain a value
			if( empty($value2) ) {
				
				$value2 = array();
				
			}
			
			
			// bail early if the current $post_id is already found in selected post's $value2
			if( in_array($post_id, $value2) ) continue;
			
			
			// append the current $post_id to the selected post's 'related_posts' value
			$value2[] = $post_id;
			
			
			// update the selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);
			
		}
	
	}
	
	
	// find posts which have been removed
	$old_value = get_field($field_name, $post_id, false);
	
	if( is_array($old_value) ) {
		
		foreach( $old_value as $post_id2 ) {
			
			// bail early if this value has not been removed
			if( is_array($value) && in_array($post_id2, $value) ) continue;
			
			
			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);
			
			
			// bail early if no value
			if( empty($value2) ) continue;
			
			
			// find the position of $post_id within $value2 so we can remove it
			$pos = array_search($post_id, $value2);
			
			
			// remove
			unset( $value2[ $pos] );
			
			
			// update the un-selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);
			
		}
		
	}
	
	
	// reset global varibale to allow this filter to function as per normal
	$GLOBALS[ $global_name ] = 0;
	
	
	// return
    return $value;
    
}

add_filter('acf/update_value/name=related_posts', 'bidirectional_acf_update_value', 10, 3);

flush_rewrite_rules( false );

/**
 * Adds a website parameter to the auth.
 *
 */

add_filter( 'jwt_auth_token_before_dispatch', 'do_something', 10, 2 );

function do_something( $data, $user ) {
	$user_info = get_user_by( 'email',  $user->data->user_email );
	$profile = array (
        'id' => $user_info->id,
        'user_first_name' => $user_info->first_name,
        'user_last_name' => $user_info->last_name,
        'user_email' => $user->data->user_email,
        'user_nicename' => $user->data->user_nicename,
        'user_display_name' => $user->data->display_name,
        'crm' => get_field( 'crm', "user_$user_info->id" ), // you also can get ACF fields
        'firstlogin' => get_field( 'firstlogin', "user_$user_info->id" ) // you also can get ACF fields
	);
	
	$response = array(
        'token' => $data['token'],
        'profile' => $profile
    );

    return $response;
	// $data['website'] = get_field( 'va', "user_$user_info->id" );
	// do_action('wp_login', $user->data->username, $user);
	// return $data;
}
