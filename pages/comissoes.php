<?php
	/**
	* Template Name: Comissoes
	*/
?>

<?php get_template_part( 'components/header' ); ?>
	<div class="section-page praticas">
		<div class="header-page">
			<div class="content-header">
				<h2 class="title"><?php echo get_the_title( wp_get_post_parent_id( get_the_ID() ) ); ?></h2>
				<p><?php echo get_post_field('post_content', wp_get_post_parent_id( get_the_ID())) ?></p>
			</div>
			<?php get_template_part( 'components/tabs-pages' ); ?>
		</div>
		<div class="tab-content">
			<?php
				$comissoes = new WP_Query( array('post_type' => array( 'comissoes' ), 'posts_per_page' => 999, 'order' => 'DESC' ));
			?>
			<ul class="list-news">
				<?php 
					if( $comissoes->have_posts() ) {
						while( $comissoes->have_posts() ):
							$comissoes->the_post();
							get_template_part( 'components/comissoes/comissao-card' );
						endwhile;
					}
				?>
			</ul>
		</div>
	</div>
<?php get_template_part( 'components/footer' ); ?>