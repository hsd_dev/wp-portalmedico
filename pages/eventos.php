<?php
	/**
	* Template Name: Educacaoomedica
	*/
?>

<?php
	//Pegando o caminho relativo à esse arquivo.
	include get_template_directory().'/components/events/filter_tabs.php';
?>

<?php get_template_part( 'components/header' ); ?>
	<div class="section-page eventos">
		<div class="header-page">
			<div class="content-header">
				<h2 class="title"><?php the_title() ?></h2>
				<p><?php echo get_the_content(); ?></p>
			</div>
			<div class="tab-list">
				<?php foreach ($months_objs as $key_month => $value) { ?>
					<div class="tab" data-month="<?php echo $months_objs[$key_month] -> title ?>"><?php echo ucfirst($months_objs[$key_month] -> title) ?></div>
				<?php } ?>
			</div>
		</div>
		<?php foreach ($months_objs as $key_month => $value): ?>
			<div class="tab-content" data-month="<?php echo $months_objs[$key_month] -> title ?>">
				<div class="list-events">
					<?php
						foreach ($months_objs[$key_month] -> posts as $key => $value) {
							$q->the_post();
							get_template_part( 'components/events/event-card' );
						}
					?>
				</div>
			</div>
		<?php endforeach ?>
	</div>
<?php get_template_part( 'components/footer' ); ?>

<script>
	$(function() {
		let currentMonth = <?php echo json_encode($months_objs[0] -> title) ?>
		
		function activeMonth(month){
			$('.section-page.eventos .tab-content').removeClass('active');
			$('.section-page.eventos .tab-list .tab').removeClass('active');
			$('.section-page.eventos .tab-content[data-month='+month+']').addClass('active');
			$('.section-page.eventos .tab-list .tab[data-month='+month+']').addClass('active');
		}

		activeMonth(currentMonth);

		$('.section-page.eventos .tab').on('click', function(event) {
			event.preventDefault();
			let currentMonth = $(this).attr('data-month');
			activeMonth(currentMonth);
		});
	});
</script>