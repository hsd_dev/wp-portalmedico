<?php
	/**
	* Template Name: Autores
	*/
?>

<?php
	$autores = new WP_Query( array(
		'post_type' => array( 'pessoas' ),
		'posts_per_page' => 999, 
		'paged'=>$paged, 
		'order' => 'DESC'
	));

	$especialidade = new WP_Query( array(
		'post_type' => array( 'especialidades' ),
		'posts_per_page' => 999, 
		'paged'=>$paged, 
		'order' => 'DESC'
	));
?>

<?php get_template_part( 'components/header' ); ?>
<div class="section-page artigos">
	<div class="header-page">
		<div class="content-header">
			<h2 class="title"><?php echo get_the_title( wp_get_post_parent_id( get_the_ID() ) ); ?></h2>
			<p><?php echo get_post_field('post_content', wp_get_post_parent_id( get_the_ID())) ?></p>
		</div>
		<?php get_template_part( 'components/tabs-pages' ); ?>
	</div>
	<div class="tab-content">
		<div class="tab-section active">
			<div class="content">
				<div class="filtro">
					<ul class="list">
						<!-- <li class="item item-icon"></li> -->
						<li class="item">
							<div class="custom-select">
								<span class="current-value">Especialidade</span>

								<select name="especialidades">
									<option default value="">Todas</option>
									<?php
										if( $especialidade->have_posts() ) {
											while( $especialidade->have_posts() ) { 
												$especialidade->the_post();
											?>
												<option value="<?php echo get_the_ID() ?>"><?php the_title() ?></option>
										<?php }
										}
									?>
								</select>
							</div>
						</li>
						<li class="item">
							<div class="custom-select">
								<span class="current-value">Autor</span>
								<select name="autores">
									<option default value="">Todos</option>
									<?php 
										if( $autores->have_posts() ) {
											while( $autores->have_posts() ) { 
												$autores->the_post();
											?>
												<option value="<?php echo get_the_ID() ?>"><?php the_title() ?></option>
											<?php }
										}
									?>
								</select>
							</div>
						</li>
						<li class="item item-date">
							<input type="text" class="datepicker" data-language='pt-BR' placeholder="Data">
						</li>
						<li class="item item-search">
							<input type="text" placeholder="Assunto">
						</li>
					</ul>
				</div>
				<ul class="list-artigos">
					<script id="entry-template" type="text/x-handlebars-template">
						<li class="artigo">
							<div class="photo"><span>{{acf.autor.abrevacao}}</span></div>
							<div class="infos">
								<div class="header-infos">
									<span class="name">{{acf.autor.post_title}}</span>
									<span class="especialidade">{{acf.especialidade.post_title}}</span>
								</div>
								<div class="wrap-infos">
									<h3 class="title">{{title.rendered}}</h3>
									<span class="description">{{text}}</span>
								</div>
								<div class="footer-infos">
									<div class="type">
										<span class="title-type">{{acf.documento.subtype}}</span>
										<span class="date">Publicado há: {{human}}</span>
									</div>
									<div class="group-buttons">
										<a class="btn-view" target="__blank" href="{{acf.documento.url}}">Visualizar</a>
										<a class="btn-download" href="{{acf.documento.url}}" download="{{acf.documento.name}}">Baixar</a>
									</div>
								</div>
							</div>
						</li>
					</script>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php get_template_part( 'components/footer' ); ?>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/moment.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/moment-with-locales.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/datepicker.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/datepicker.pt-BR.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/object-path/index.js"></script>

<script type="text/javascript">
	let source = document.getElementById("entry-template").innerHTML;
	let template = Handlebars.compile(source);

	// var objectPath = require("../assets/js/object-path");

	moment.locale('pt-br');

	Array.prototype.remove = function() {
		var what, a = arguments, L = a.length, ax;
		while (L && this.length) {
			what = a[--L];
			while ((ax = this.indexOf(what)) !== -1) {
				this.splice(ax, 1);
			}
		}
		return this;
	};

	const autores = {
		data: {
			currentFilter: [],
			temp: [],
			results: [],
			found_obj: [],
			allPosts: null
		},
		methods: {
			getPosts(){
				axios.get('/wp-json/wp/v2/artigos')
				.then((response) => {
					autores.methods.renderPosts(response);
				})
				.catch((error) => {
					// handle error
					console.log(error);
				})
				.then(() => {
					// always executed
				});
			},
			renderPosts(data){
				let html = [];
				autores.data.allPosts = data.data;
				autores.data.temp = data.data;

				// axios.get('/wp-json/wp/v2/pessoas?_embed')
				// .then((response) => {
				// 	// val.acf.autor.thumb = response.data._links['wp:featuredmedia'][0].href;
				// 	console.log(response.data)
				// })
				// .catch((error) => {
				// 	console.log(error);
				// })
				// .then(() => {
				// });

				const getInitials = name => {
					let initials = '';
					name.split(' ').map( subName => initials = initials + subName[0]);
					return initials;
				};

				$.each(data.data, function(index, val) {
					val.text = val.title.rendered.replace(/<\/?[^>]+(>|$)/g, "");
					val.acf.autor = val.acf.autor[0];
					val.acf.id = val.acf.autor.ID;
					val.acf.especialidade = val.acf.especialidade[0];
					val.human = moment(val.modified).fromNow();
					val.acf.autor.abrevacao = getInitials(val.acf.autor.post_title);

					// $.ajax({
					// 	type: "GET",
					// 	url: '<?php bloginfo('url');?>/wp-content/themes/wp-portalmedico/pages/get_thumb.php',
					// 	dataType: 'json',
					// 	data: {id: val.acf.id},
					// 	success: (result) => {
					// 		val.acf.autor.thumb = result
					// 		// console.log(val.thumb.result)
					// 	}
					// });

					// console.log(val)
					html.push(template(val))
				});

				$('.list-artigos').html(html);
			},
			filterArtigos(data){
				let currentIndex = autores.data.currentFilter.find(n => n.collect == data.collect)
				
				if (currentIndex && data.value) {
					currentIndex.value = data.value
				}else if(!data.value){
					let index = autores.data.currentFilter.findIndex(x => x.collect === data.collect)
					autores.data.currentFilter.splice(index, 1)
				}else{
					autores.data.currentFilter.push(data)
				}

				if (autores.data.currentFilter.length) {
					let results = [];
					let currentFirtsFilter = null

					function compare(elem, value, collect){
						if (collect == 'date') {
							return moment(String(objectPath.get(elem, collect))).format('DD/MM/YYYY') === value;
						}else if(collect == 'text'){
							return String(objectPath.get(elem, collect)).toLowerCase().startsWith(value);
						}else{
							return String(objectPath.get(elem, collect)) === value;
						}
					}

					let currentArr = autores.data.allPosts.filter((elem) => compare(elem, autores.data.currentFilter[0].value, autores.data.currentFilter[0].collect));

					$.map(autores.data.currentFilter, function(filter, index) {
						if (index >= 1) {
							autores.data.results = autores.data.results.filter((elem) => compare(elem, filter.value, filter.collect))

						}else{
							autores.data.results = currentArr;
						}
					});
				}else{
					autores.data.results = autores.data.allPosts;
				}

				autores.methods.renderHtml()
			},
			renderHtml(){
				let html = [];
				$.each(autores.data.results, function(index, post) {
					html.push(template(post))
				});

				$('.list-artigos').html(html);
			}
		},
		events(){
			$('select[name="autores"]').change(function(e){
				let currentOption = ''

				if (this.value) {
					currentOption = $(this).children('option').filter(':selected').text();
				}else{
					currentOption = 'Autores'
				}


				$(this).closest('.custom-select').children('.current-value').text(currentOption)
				autores.methods.filterArtigos({collect: 'acf.autor.ID', value: this.value});
			})

			$('select[name="especialidades"]').change(function(e){
				let currentOption = ''
				
				if (this.value) {
					currentOption = $(this).children('option').filter(':selected').text();
				}else{
					currentOption = 'especialidades'
				}

				$(this).closest('.custom-select').children('.current-value').text(currentOption)
				autores.methods.filterArtigos({collect: 'acf.especialidade.ID', value: this.value});
			})

			$('.item-search input').on('keyup', function(event) {
				event.preventDefault();
				autores.methods.filterArtigos({collect: 'text', value: this.value});
			});

			$('.datepicker').datepicker({
				onSelect(formattedDate, date, inst){
					autores.methods.filterArtigos({collect: 'date', value: formattedDate});
				}
			})
		},
		init(){
			this.methods.getPosts();
			this.events();
		}
	}

	autores.init();
</script>