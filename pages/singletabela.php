<?php
	/**
	* Template Name: Single Tabelas
	*/
?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/styles/dist/normas.css">

<?php get_template_part( 'components/header' ); ?>
<div class="section-page normas">
	<div class="header-page">
		<div class="content-header">
			<h2 class="title"><?php echo get_the_title( wp_get_post_parent_id( get_the_ID() ) ); ?></h2>
			<p><?php echo get_post_field('post_content', wp_get_post_parent_id( get_the_ID())) ?></p>
		</div>
		<div class="menu-principal-container">
			<ul id="menu-principal-1" class="menu">
				<li class="menu-item current-menu-item">
					<a href="#" aria-current="page">Documentos</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="tab-content">
		<div class="tab-section active">
			<div class="content">
				<?php if( have_rows('files') ): ?>
					<ul class="list-dowloads">
						<?php while( have_rows('files') ): the_row();?>

						<!-- <li class="dowload"><?php echo $file ?></li> -->
						<?php get_template_part( 'components/normas/download-card' ); ?>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_template_part( 'components/footer' ); ?>
<script type="text/javascript">
</script>