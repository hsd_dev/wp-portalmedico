<?php
	require_once("../../../../wp-load.php");

	$thumb_id = $_GET['id'];
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
	$thumb_url = $thumb_url_array[0];

	// echo $posts;
	// $thumb_url_array = wp_get_attachment_image_src(981);
	// $thumb_url = $thumb_url_array[0];

	echo json_encode(array("result" => $thumb_url));
?>