<?php
	/**
	* template name: Sem Jaleco
	*/
?>
<?php get_template_part( 'components/header' ); ?>
<div class="section-page news news-semjaleco">
	<div class="header-page">
		<div class="content-header">
			<h2 class="title"><?php echo get_the_title( wp_get_post_parent_id( get_the_ID() ) ); ?></h2>
			<p><?php echo get_post_field('post_content', wp_get_post_parent_id( get_the_ID())) ?></p>
		</div>
		<?php get_template_part( 'components/tabs-pages' ); ?>
	</div>
	<div class="tab-content">
		<div class="header-section-news">
		    <h3 class="title">Conheça um pouco sobre os hábitos, <strong>hobby, gostos e outros detalhes</strong> da vida pessoal de nossos médicos
		    </h3>
		    <div class="icon">
		        <div class="line"></div>
		    </div>
		    <p>Entenda suas rotinas, desafios e segredos da profissão.</p>
		</div>
		<?php
			$q = new WP_Query( array('post_type' => array( 'semjaleco' ),'posts_per_page' => 999, 'paged'=>$paged, 'order' => 'DESC' ));
		?>
		<ul class="list-edicoes">
			<?php 
				if( $q->have_posts() ) {
					while( $q->have_posts() ) {
						$q->the_post();
						get_template_part( 'components/news/edicao-card' );
					}
				}
			?>
		</ul>
	</div>
</div>
<?php get_template_part( 'components/footer' ); ?>