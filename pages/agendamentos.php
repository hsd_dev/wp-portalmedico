<?php
	/**
	* Template Name: Minha agenda
	*/
?>
<?php
	// echo $_COOKIE['user-token'];
	if (!$_COOKIE['user-token']) {
		wp_redirect( home_url() );
		exit;
	}
?>
<?php get_template_part( 'components/header' ); ?>
	<div class="section-page auth-agendamentos">
		<div class="header-page">
			<div class="content-header">
				<h2 class="title"><?php echo get_the_title( $page->ID ) ?></h2>
				<p><?php echo get_post_field('post_content', $page->ID) ?></p>
			</div>
			<?php get_template_part( 'components/tabs-pages' ); ?>
		</div>
		<div class="tab-content">
			<ul class="list-agendamentos">
				<script id="entry-template" type="text/x-handlebars-template">
					<li class="item">
						<div class="header-itemAgenamento">
							<span class="date-time">{{time}}</span>
							<span class="especialidade">{{specialty}}</span>
						</div>
						<div class="content-item">
							<h3 class="title">{{name}}</h3>
							<div class="footer-content">
								<div class="data-user">
									<span class="info-nascimento">{{sex}} - {{idade}} anos</span>
									<span class="info-atendimento">{{encounter}}</span>
								</div>
								<a class="link" href="{{link}}">Entrar na sala</a>
							</div>
						</div>
					</li>
				</script>
			</ul>
			<div class="notfound-agendamentos">
				<div class="icon"></div>
				<h3 class="title-notfound">Você não tem nada pra hoje</h3>
				<span class="description">Verificamos sua agenda, e não encontramos nenhuma consulta</span>
			</div>
			<div class="aside-agendamentos">
				<div class="date-time">
					<span class="today">Hoje</span>
					<span class="date">Seg, 25 de março</span>
				</div>
				<div class="wrap-counter">
					<div class="icon"></div>
					<div class="content-counter">
						<span class="title-counter">Total de consultas para hoje.</span>
						<span class="description">Fique atento!</span>
					</div>
					<div class="number-counter">
						<h3 class="number">20</h3>
						<span class="description">Consultas</span>
					</div>
				</div>
				<div class="tips">
					<ul class="list">
						<li class="item">
							<div class="icon lamp"></div>
							<p class="text-tip">Tente ir para um lugar com boa iluminação.</p>
						</li>
						<li class="item">
							<div class="icon clock"></div>
							<p class="text-tip">Seja pontual com sua consulta.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php get_template_part( 'components/footer' ); ?>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/moment.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/moment-with-locales.js"></script>

<script type="text/javascript">
	let source = document.getElementById("entry-template").innerHTML;
	let template = Handlebars.compile(source);

	moment.locale('pt-br');

	const agendamentos = {
		data: {
			allPosts: [
				// {
				// 	time: '12:00',
				// 	name: 'Carla amorim pinto',
				// 	sex: 'feminino',
				// 	birthdate: '02/05/1992',
				// 	encounter: '12312333',
				// 	link: 'www.google.com.br'
				// }
			]
		},
		methods: {
			getPosts(){
				let idUser = localStorage.getItem('user-id');
				const baseUrl = 'https://teleconsulta.hospitalsaodomingos.com.br/api/teleconsulta/scheduling/virtual/'

				axios.get(baseUrl + idUser, {
					auth: { 
						username: 'hsdservice', 
						password: 'j!6#gs1lWVyn'
					}
				})
				.then((response) => {
					agendamentos.data.allPosts = response.data.data;
					agendamentos.methods.renderPosts(response.data.data);

					agendamentos.methods.renderAside();
				})
				.catch((error) => {
					console.log(error);
				})
				.then(() => {
					// always executed
				});
			},
			renderAside(){
				$('.aside-agendamentos .date-time .date').text(moment(new Date).format('LL'))
				console.log(agendamentos.data.allPosts)
				$('.wrap-counter .number-counter .number').text(agendamentos.data.allPosts.length)
			},
			renderPosts(data){
				if (!data.length) {
					$('.notfound-agendamentos').addClass('show');
					$('.list-agendamentos').removeClass('show');
				}else{
					let html = [];

					$.each(data, function(index, val) {
						let birthDay = val.birthdate;
						let age = Math.floor(moment(new Date()).diff(moment(birthDay),'years',true));

						val.idade = age

						html.push(template(val))
					});

					$('.list-agendamentos').html(html);

					$('.list-agendamentos').addClass('show');
					$('.notfound-agendamentos').removeClass('show');
				}

			}
		},
		events(){
			
		},
		init(){
			this.methods.getPosts();
			this.events();
		}
	}

	agendamentos.init();
</script>