<?php
	/**
	* template name: praticas
	*/
?>
<?php
	$pageids = array();
	$pages = get_pages('child_of='.$post->ID);

	foreach($pages as $page){
		$pageids[] = $page->ID;
	}

	redirect(get_permalink( $pageids[0] ));
?>