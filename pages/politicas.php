<?php
	/**
	* Template Name: Politicas
	*/
?>

<?php get_template_part( 'components/header' ); ?>
	<div class="section-page praticas">
		<div class="header-page">
			<div class="content-header">
				<?php $page = get_page_by_title( 'Práticas Médicas' ); ?>
				<h2 class="title"><?php echo get_the_title( $page->ID ) ?></h2>
				<p><?php echo get_post_field('post_content', $page->ID) ?></p>
			</div>
			<?php get_template_part( 'components/tabs-pages' ); ?>
		</div>
		<div class="tab-content">
		</div>
	</div>
<?php get_template_part( 'components/footer' ); ?>