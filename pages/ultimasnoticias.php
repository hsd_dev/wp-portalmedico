<?php
	/**
	* Template Name: ultimas noticias
	*/
?>

<?php get_template_part( 'components/header' ); ?>
<div class="section-page news">
	<div class="header-page">
		<div class="content-header">
			<h2 class="title"><?php echo get_the_title( wp_get_post_parent_id( get_the_ID() ) ); ?></h2>
			<p><?php echo get_post_field('post_content', wp_get_post_parent_id( get_the_ID())) ?></p>
		</div>
		<?php get_template_part( 'components/tabs-pages' ); ?>
	</div>
	<div class="tab-content">
		<div class="tab-section active">
			<?php
				$q = new WP_Query( array('post_type' => array( 'post' ),'posts_per_page' => 999, 'paged'=>$paged, 'order' => 'DESC' ));
			?>
			<ul class="list-news destaque">
				<?php 
					if( $q->have_posts() ) {
						while( $q->have_posts() ) {
							$q->the_post();
							get_template_part( 'components/news/new-card' );
						}
					}
				?>
			</ul>
		</div>
	</div>
</div>
<?php get_template_part( 'components/footer' ); ?>

<script type="text/javascript">
</script>