<?php
	/**
	* Template Name: Diretorias
	*/
?>

<?php get_template_part( 'components/header' ); ?>
	<div class="section-page praticas">
		<div class="header-page">
			<div class="content-header">
				<h2 class="title"><?php echo get_the_title( wp_get_post_parent_id( get_the_ID() ) ); ?></h2>
				<p><?php echo get_post_field('post_content', wp_get_post_parent_id( get_the_ID())) ?></p>
			</div>
			<?php get_template_part( 'components/tabs-pages' ); ?>
		</div>
		<div class="tab-content">
			<?php
				$q = new WP_Query( array('post_type' => array( 'diretorias' ),'posts_per_page' => 999, 'paged'=>$paged, 'order' => 'DESC' ));
			?>
			<ul class="list-news">
				<?php 
					if( $q->have_posts() ) {
						while( $q->have_posts() ) {
							$q->the_post();
							get_template_part( 'components/diretorias/diretoria-card' );
						}
					}
				?>
			</ul>
		</div>
	</div>
<?php get_template_part( 'components/footer' ); ?>