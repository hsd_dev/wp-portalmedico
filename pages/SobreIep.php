<?php
    /**
    * Template Name: Sobre IEP
    */
?>

<?php get_template_part( 'components/header' ); ?>
<div class="section-page iep">
    <div class="header-page">
        <div class="content-header">
            <h2 class="title"><?php echo get_the_title( wp_get_post_parent_id( get_the_ID() ) ); ?></h2>
            <p><?php echo get_post_field('post_content', wp_get_post_parent_id( get_the_ID())) ?></p>
        </div>
        <?php get_template_part( 'components/tabs-pages' ); ?>
    </div>
    <div class="tab-content">
        <div class="tab-section active">
            <?php if ( has_post_thumbnail()) : ?>
                <?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
                <img class="thumb" src="<?php echo $thumbnail[0]; ?>"></img>
                <?php else : ?>
                <div class="thumb no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
            <?php endif; ?>

            <div class="content">
                <?php echo the_content() ?>
            </div>
            
            <div class="mais-informacoes">
                <h3 class="title">Mais informações</h3>
                <ul class="list">
                    <li class="item phone">
                        <div class="icon"></div>
                        <div class="content-item">
                            <span class="description">Telefone</span>
                            <span class="value"><?php echo the_field('telefone_iep') ?></span>
                        </div>
                    </li>
                    <li class="item email">
                        <div class="icon"></div>
                        <div class="content-item">
                            <span class="description">Email</span>
                            <span class="value"><?php echo the_field('email_iep') ?></span>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="local">
                <div class="group">
                    <div class="header-local">Local</div>
                    <div class="wrap-local">
                        <div class="icon"></div>
                        <div class="content-local">
                            <span><?php echo the_field('local_iep') ?></span>
                        </div>
                    </div>
                </div>
                <div class="map"></div>
            </div>
        </div>
    </div>
</div>
<?php get_template_part( 'components/footer' ); ?>

<script type="text/javascript">
</script>