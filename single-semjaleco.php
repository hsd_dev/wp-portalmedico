<?php if ( has_post_thumbnail()) : ?>
	<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
		<?php set_query_var('bg', $thumbnail[0]); ?>
	<?php else : ?>
		<?php set_query_var('bg', null); ?>
<?php endif; ?>


<?php
	$q = new WP_Query( array('post_type' => array( 'semjaleco' )));
	$editions = Array();

	class Edition{
		public $title;
		public $link;
		public $id;
	}

	if( $q->have_posts() ) {
		while( $q->have_posts() ) {
			$q->the_post();
			$edition = new Edition();
			$edition->title = get_field('numero_da_edicao');
			$edition->link = get_permalink();
			$edition->id = $post->ID;

			array_push($editions, $edition);
		}
	}
?>

<?php get_template_part( 'components/header' ); ?>
	<div class="container-page post-inner semjaleco-inner">
		<div class="header-inner">
			<button class="btn-back"></button>
			<div class="tab-list">
				<?php foreach ($editions as $key => $value): ?>
					<?php $current_class = ( $editions[$key] -> id == $post->ID ) ? 'active' : ''; ?>

					<div class="tab <?php if ( $current_class ) echo $current_class; ?>">
						<a href="<?php echo $editions[$key] -> link ?>"><?php echo $editions[$key] -> title ?> Edição</a>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<div class="post">
			<div class="header-post">
				<div class="top">
					<span class="type">Sem Jaleco</span>
					<h2 class="title"><?php the_title() ?></h2>
					<p class="description">Saiba como é sua vida fora do hospital</p>
				</div>
				<div class="bottom">
					<div class="edition">
						<div class="group">
							<span class="month">Nº</span>
							<span class="description">edição</span>
						</div>
						<h3 class="number">#<?php the_field('numero_da_edicao') ?></h3>
					</div>
					<span class="publication">
						<span class="description-date">Publicado há:</span>
						<time class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp')) ?></time>
					</span>
				</div>
			</div>

			<div class="content-post">
				<div class="header-content-post">
					<div class="group-name">
						<span class="description">conhecendo</span>
						<h3 class="title"><?php the_title() ?></h3>
					</div>
					<div class="row row-intro">
						<div class="row row-text">
							<?php the_content() ?>
						</div>

						<ul class="list-infos">
							<?php
								$sobre = get_field('sobre');
								$nascimento = $sobre['nascimento'];
								$altura = $sobre['altura'];
								$naturalidade = $sobre['naturalidade'];
							?>
							<li class="info">
								<span class="text"><strong>Aniversário:</strong> <?php echo $nascimento ?> </span>
							</li>
							<li class="info">
								<span class="text"><strong>Naturalidade:</strong> <?php echo $naturalidade ?> </span>
							</li>
						</ul>
					</div>
				</div>
				<div class="row row-hobby">
					<?php
						$hobby = get_field('hobby');
						$frase = $hobby['frase_hobby'];
						$texto_hobby = $hobby['texto_hobby'];
						$photo_hobby = $hobby['photo_hobby']['url'];
					?>
					<div class="column">
						<div class="photo">
							<img src="<?php echo esc_url( $photo_hobby ) ?>" alt="">
						</div>
					</div>
					<div class="column column-hobby">
						<div class="content-row">
							<span class="title-row">Hobby</span>
							<h3 class="title-bg">
								<div class="highlight">
									<?php echo $frase; ?>
								</div>
							</h3>
						</div>
					</div>
				</div>
				<div class="row row-text">
					<?php echo $texto_hobby ?>
				</div>
				<div class="row">
					<?php
						$motivacao = get_field('motivacao');
						$texto_motivacao = $motivacao['texto_motivacao'];
						$photo_motivacao = $motivacao['foto_motivacao']['url'];

						$sonho = get_field('sonho');
						$texto_sonho = $sonho['texto_sonho'];
						$photo_sonho = $sonho['photo_sonho']['url'];
					?>
					<div class="column">
						<div class="content-row motivacao">
							<span class="title-row">Motivação</span>
							<?php echo $texto_motivacao ?>
						</div>
						<div class="photo">
							<img src="<?php echo esc_url( $photo_motivacao ) ?>" alt="">
						</div>
					</div>
					<div class="column">
						<div class="photo">
							<img src="<?php echo esc_url( $photo_sonho ) ?>" alt="">
						</div>
						<div class="content-row sonho">
							<span class="title-row">Sonho</span>
							<?php echo $texto_sonho ?>
						</div>
					</div>
				</div>
				<div class="row">
					<?php if( have_rows('extras') ): ?>

						<ul class="list-extra">

						<?php while( have_rows('extras') ): the_row();
							$titulo_extra = get_sub_field('titulo_extra');
							$texto_extra = get_sub_field('texto_extra');
							$imagem_extra = get_sub_field('imagem_extra');
							$tipo_extra = get_sub_field('tipo_extra');
							$titulo_conteudo_extra = get_sub_field('titulo_conteudo_extra');
						?>

							<li class="item">
								<div class="column-item">
									<span class="title-item"><?php echo $titulo_extra ?></span>
									<p class="text-item"><?php echo $texto_extra ?></p>
								</div>
								<div class="column-item">
									<div class="thumb" style="background-image: url(<?php echo $imagem_extra['url']; ?>)"></div>
									<div class="infos">
										<span class="type"><?php echo $tipo_extra ?></span>
										<span class="title"><?php echo $titulo_conteudo_extra ?></span>
									</div>
								</div>

							</li>

						<?php endwhile; ?>

						</ul>

					<?php endif; ?>
				</div>
			</div>
			<div class="footer-post">
				<h3 class="title-bg">
					<div class="highlight">
						Somos um time repleto de pessoas fantasticas
					</div>
				</h3>
				<?php
					$redes_sociais = get_field('redes_sociais');
					$facebook = $redes_sociais['facebook'];
					$instagram = $redes_sociais['instagram'];
					$linkedin = $redes_sociais['linkedin'];
				?>
				<?php if ($facebook || $instagram || $linkedin): ?>
					<div class="follow">
						<span class="username">Siga o <strong class="name">@<?php echo the_title() ?></strong></span>
						<ul class="list-redes">
							<?php if ($instagram): ?>
								<li class="rede instagram">
									<a href="<?php echo $instagram; ?>">Instagram</a>
								</li>
							<?php endif ?>
							<?php if ($facebook): ?>
								<li class="rede facebook">
									<a href="<?php echo $facebook; ?>">Facebook</a>
								</li>
							<?php endif ?>
							<?php if ($linkedin): ?>
								<li class="rede linkedin">
									<a href="<?php echo $linkedin; ?>">Linkedin</a>
								</li>
							<?php endif ?>
						</ul>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
<?php get_template_part( 'components/footer' ); ?>