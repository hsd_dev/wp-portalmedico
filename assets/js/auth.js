$(function() {
	let currentUser = {
		username: null,
		password: 123
	}

	let resultScore = null;

	let newUser = {
		user: null,
		token: null,
		password: null
	}

	let errors = {
		notfound: 'Não foi possível encontrar sua Conta do HSD.',
		invalid: 'Digite um número de CPF válido',
		password: 'Senha incorreta. Tente novamente.'
	}

	const auth = {
		init(){
			this.methods.initAuth();
			this.events();
		},
		events () {
			// $('.step-username').submit(function(e){
			// 	e.preventDefault();
			// });

			// $('.step-password').submit(function(e){
			// 	return false;
			// });

			$('.wrap-input .input-cpf').mask('000.000.000-00', {reverse: true});

			$('.wrap-input input').focus(function (e) { 
				e.preventDefault();
				auth.methods.focusInput($(this));
			});

			$('.wrap-input input').blur(function (e) { 
				e.preventDefault();
				auth.methods.blurInput($(this));
			});

			$('.wrap-input .input-cpf').keyup(function (e) {
				e.preventDefault();
				currentUser.username = $(this).val().replace(/[^\d]+/g,'');
				auth.methods.validadeUserName();
			});

			$('.wrap-input .input-password').keyup(function (e) {
				e.preventDefault();
				currentUser.password = $(this).val();
				auth.methods.validadePassword();
			});

			$('.btn-proximo').click(function (e) {
				e.preventDefault();
				auth.methods.signinCPF();
			});

			$('.btn-entrar').click(function (e) {
				e.preventDefault();
				auth.methods.signinPassword();
			});

			$('.btn-new').click(function (e) {
				e.preventDefault();
				auth.methods.updatePassword();
			});

			$('.btn-backcpf').click(function (e) {
				e.preventDefault();
				auth.methods.backCPF();
			});

			$('.btn-login').click(function (e) {
				e.preventDefault();
				auth.methods.openLogin();
			});

			$('.button-close').click(function (e) {
				e.preventDefault();
				auth.methods.closeLogin();
			});

			$('.button-close').click(function (e) {
				e.preventDefault();
				auth.methods.closeLogin();
			});

			$('.btn-logout').click(function (e) {
				e.preventDefault();
				auth.methods.logout();
			});

			$('.form-login .overlay').click(function (e) {
				e.preventDefault();
				auth.methods.closeLogin();
			});

			// auth.methods.firstLogin();
		},
		methods: {
			logout(){
				localStorage.clear();
				Cookies.remove('user-token')
				// auth.methods.deleteCookie('user-token');
				auth.methods.initAuth();
				location.reload();
			},
			renderDataUser(){
				$('.header-auth .item-auth a').text(localStorage.getItem('nameuser'))
				$('.group-auth .infos .title').text(localStorage.getItem('nameuser'))
				$('.group-auth .infos .description').text('CRM: '+localStorage.getItem('crm'))
			},
			initAuth(){
				if (Cookies.get('user-token')) {
					$('body').addClass('auth');
					auth.methods.validateToken();

					$('.tools-menu-mobile .wrap-login .infos .title').text(localStorage.getItem('nameuser'))
					$('.tools-menu-mobile .wrap-login .infos .description').text('CRM: '+localStorage.getItem('crm'))
				}else{
					$('body').removeClass('auth')

					$('.tools-menu-mobile .wrap-login .infos .title').text('Acesse sua conta')
					$('.tools-menu-mobile .wrap-login .infos .description').text('Faça login para você ver sua agenda do Hospital São Domingos.')
				}

				auth.methods.renderDataUser();
			},
			validateToken(){
				const token = Cookies.get('user-token')
				axios.defaults.headers.common = {Authorization: 'Bearer ' + token}
				// headers: {Authorization: 'Bearer ' + localStorage.token}}

				axios.post('/wp-json/jwt-auth/v1/token/validate')
				.then((response) => {
					$('body').addClass('auth');
				})
				.catch((error) => {
					auth.methods.logout();
					Cookies.remove('user-token');
					$('body').removeClass('auth')
				});
			},
			openLogin(){
				$('.form-login').addClass('show')
			},
			closeLogin(){
				auth.methods.resetForm();
				$('.container-form').attr('data-step', 'username');
				$('.form-login').removeClass('show')
			},
			updatePassword(){
				$('.step-firstlogin .loading').addClass('show');

				if (auth.methods.validadeNewPassword()) {
					axios.defaults.headers.common = {'Authorization': `Bearer ${newUser.token}`}

					const url = '/wp-json/wp/v2/users/'+newUser.user+'?password='+newUser.password+'';

					axios.post(url)
					.then((response) => {
						$('.step-firstlogin .loading').removeClass('remove');
						auth.methods.getCookieTemp();
					})
					.catch((error) => {
						$('.step-firstlogin .loading').removeClass('remove');
					})
					.then(() => {
					});
				}
			},
			getCookieTemp(){
				axios.post('/api/user/generate_auth_cookie?username='+currentUser.username+'&password='+newUser.password+'&insecure=cool')
				.then((response) => {
					auth.methods.putFlagFirstLogin(response.data);
				})
				.catch((error) => {
				})
				.then(() => {
				});
			},
			putFlagFirstLogin(user){
				axios.post('/api/user/update_user_meta_vars?cookie='+user.cookie+'&firstlogin=0&insecure=cool')
				.then((response) => {
					auth.methods.backCPF();
				})
				.catch((error) => {
				})
				.then(() => {
				});
			},
			firstLogin(){
				var meter = $('.password-strength-meter');
				var text = $('.password-strength-text');

				var strength = {
					0: "Muito fraca",
					1: "Ainda fraca",
					2: "Fraca",
					3: "Boa senha",
					4: "Excelente Senha"
				}

				$('.input-newPassword').keyup(function(e) {
					var val = $(this).val();
					resultScore = zxcvbn(val);

					newUser.password = val;

					// // Update the password strength meter
					meter.attr('data-value', resultScore.score);

					if (resultScore.score >= 2) {
						$('.step-firstlogin .btn-new').attr('data-validate', true);
					}else{
						$('.step-firstlogin .btn-new').attr('data-validate', false);
					}
					// // Update the text indicator
					if (val !== '') {
						text.text(strength[resultScore.score]);
					} else {
						text.text('');
					}
				});
			},
			saveDataUser(data){
				if (data.profile.firstlogin) {
					$('.container-form').attr('data-step', 'firstlogin')
					$('.container-form .header-form .title').text('Redefinição de senha')
					$('.container-form .header-form .description').text('Você será redirecionado ao login novamente, assim que redefinir sua senha, por questões de segurança');

					newUser.user = data.profile.id;
					newUser.token = data.token;

					$('.step-firstlogin .description-first').text(data.profile.user_display_name);

					auth.methods.firstLogin();
				}else{
					localStorage.setItem('nameuser', data.profile.user_display_name);
					localStorage.setItem('crm', data.profile.crm);
					localStorage.setItem('user-token', data.token);
					localStorage.setItem('user-id', data.profile.user_nicename);

					Cookies.set('user-token', data.token, { expires: 1 })

					auth.methods.initAuth();
					auth.methods.closeLogin();
				}
			},
			signinCPF(){
				$('.step-username .loading').addClass('show');
				axios.post('/wp-json/jwt-auth/v1/token', currentUser)
				.then((response) => {
				})
				.catch((error) => {
					$('.step-username .loading').removeClass('show');

					let currentCode = error.response.data.code.replace('[jwt_auth] ', '')

					if (currentCode === 'incorrect_password') {
						$('.container-form').attr('data-step', 'password')
						$('.step-password .header-user .infos .title').text(currentUser.username.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g,"\$1.\$2.\$3\-\$4"))
					}else{
						$('.step-username.content-form').addClass('isError');

						$('.erro-username').addClass('show')
						$('.erro-username').text(errors.notfound);
					}
				})
				.then(() => {
				});
			},
			testeUser(){
				let post = {
					"title": 'aosiudasiod',
					"status": 'publish'
				}

				axios.post('/wp-rest/wp-json/wp/v2/posts', currentUser,{
					auth: {
						username: currentUser.username, 
						password: currentUser.password
					}
				})
				.then((response) => {
					console.log(response)
				})
				.catch((error) => {
				})
				.then(() => {
				});
			},
			signinPassword(){
				if (auth.methods.validadePassword()) {
					$('.step-password .loading').addClass('show');

					axios.post('/wp-json/jwt-auth/v1/token', currentUser)
					.then((response) => {
						$('.step-password .loading').removeClass('show');
						auth.methods.saveDataUser(response.data);
						// auth.methods.testeUser();
					})
					.catch((error) => {
						$('.step-password .loading').removeClass('show');

						let currentCode = error.response.data.code.replace('[jwt_auth] ', '')

						if (currentCode === 'incorrect_password') {
							$('.step-password.content-form').addClass('isError');

							$('.erro-password').addClass('show');
							$('.erro-password').text(errors.password);
						}
					})
					.then(() => {
					});
				}else{
					$('.step-password.content-form').addClass('isError');
					
					$('.erro-password').addClass('show');
					$('.erro-password').text('Senhas de no minimo 4 digitos');
				}
			},
			resetForm(){
				currentUser.username = null
				currentUser.password = 123
				$('.wrap-input .input-cpf').val('')
				$('.wrap-input .input-password').val('')

				$('.container-form .header-form .title').text('Acesso a área privada')
				$('.container-form .header-form .description').text('Área destinada apenas para profissionais do hospital São Domingos');

				$('.step-password .loading').removeClass('show');
				$('.step-username .loading').removeClass('show');
			},
			backCPF(){
				auth.methods.resetForm();
				$('.container-form').attr('data-step', 'username');
			},
			validaCPF(currentCpf){
				var regex = /^[0-9]{11}$/;

				if (currentCpf.match(regex) != null) {
					//check all same numbers
					if (currentCpf.match(/\b(.+).*(\1.*){10,}\b/g) != null)
						return false;

					var strCPF = currentCpf.replace(/\D/g, '');
					var sum;
					var rest;
					sum = 0;

					for (i = 1; i <= 9; i++)
						sum = sum + parseInt(strCPF.substring(i - 1, i)) * (11 - i);

					rest = (sum * 10) % 11;

					if ((rest == 10) || (rest == 11))
						rest = 0;

					if (rest != parseInt(strCPF.substring(9, 10)))
						return false;

					sum = 0;
					for (i = 1; i <= 10; i++)
						sum = sum + parseInt(strCPF.substring(i - 1, i)) * (12 - i);

					rest = (sum * 10) % 11;

					if ((rest == 10) || (rest == 11))
						rest = 0;
					if (rest != parseInt(strCPF.substring(10, 11)))
						return false;

					return true;
				}

				return false;
			},
			validadeUserName(){
				if (currentUser.username.length >= 11) {
					validateCpf = auth.methods.validaCPF(currentUser.username)
					if (validateCpf) {
						$('.btn-proximo').attr('data-validate', true);
					}else{
						$('.step-username.content-form').addClass('isError');

						$('.erro-username').addClass('show')
						$('.erro-username').text(errors.invalid);
						$('.btn-proximo').attr('data-validate', false);
					}
				}else{
					$('.erro-username').text('');

					$('.step-username.content-form').removeClass('isError');

					$('.erro-username').removeClass('show')
					$('.btn-proximo').attr('data-validate', false);
				}
			},
			validadePassword(){
				if (currentUser.password.length >= 4) {
					$('.btn-entrar').attr('data-validate', true);
					return true;
				}else{
					$('.btn-entrar').attr('data-validate', false);
					
					$('.step-password.content-form').removeClass('isError');

					$('.erro-password').removeClass('show');
					$('.erro-password').text('');
					return false;
				}
			},
			validadeNewPassword(){
				if (resultScore.score >= 2) {
					return true;
				}else{
					return false;
				}
			},
			focusInput(el){
				const value = el.val().trim()
				if(!value){
					el.closest('.wrap-input').addClass('focus');
				}
			},
			blurInput(el){
				const value = el.val().trim()
				if(!value){
					el.closest('.wrap-input').removeClass('focus');
				}
			}
		}
	}

	auth.init();
});