$(function() {
	const app = {
		init(){
			this.events();
		},
		events () {
			$('.button-mobile').on('click', function(event) {
				event.preventDefault();
				app.methods.openMenu();
			});

			$('.btn-cadastro, .wrap-cadastro').on('click', function(event) {
				event.preventDefault();
				app.methods.openCadastro();
			});

			$('.form-cadastro .overlay').on('click', function(event) {
				event.preventDefault();
				app.methods.closeCadastro();
			});

			$('.btn-close, .menu-item a').on('click', function(event) {
				app.methods.closeMenu();
			});

			$('.btn-back').on('click', function(event) {
				history.go(-1);
			});

			$('.container-form .btn-close, .btn-close-fim').on('click', function(event) {
				event.preventDefault();
				app.methods.closeCadastro();
			});
		},
		methods: {
			openCadastro(){
				$('.form-cadastro').addClass('show-cadastro');
			},
			closeCadastro(){
				$('.form-cadastro').removeClass('show-cadastro');
				$('body').attr('data-currentstep', 0);
				
				$('.step').removeClass('step-show');
				$('.step[data-id="INTRO"]').addClass('step-show');

				$('.form-cadastro').trigger('reset');
			},
			openMenu(){
				$('.main-menu').addClass('show-mobile');
				$('html').addClass('no-scroll');
			},
			closeMenu(){
				$('.main-menu').removeClass('show-mobile');
				$('html').removeClass('no-scroll');
			}
		}
	}

	app.init();
});