$(function() {
	const middleware = {
		init(){
			this.methods.initAuth();
			this.events();
		},
		events () {
		},
		methods: {
			initAuth(){
				if (!localStorage.getItem('user-token')) {
					window.location.href = '/'
				}
			}
		}
	}

	middleware.init();
});