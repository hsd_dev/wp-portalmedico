<?php get_template_part( 'components/header' ); ?>
	<div class="container-page post-inner noticia-inner">
		<div class="header-inner">
			<button class="btn-back"></button>
			<div class="breadcrumbs">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb();
					}
				?>
			</div>
		</div>
		<div class="post">
			<?php if ( has_post_thumbnail()) : ?>
				<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
				<img class="thumb" src="<?php echo $thumbnail[0]; ?>"></img>
				<?php else : ?>
				<div class="thumb no-image", style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/no-image.svg)"></div>
			<?php endif; ?>
			<div class="content">
				<div class="header-content">
					<h2 class="title"><?php echo the_title() ?></h2>
					<span class="publication">
						<span class="description-date">Publicado há:</span>
						<time class="date"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp')) ?></time>
					</span>
				</div>
				<div class="text">
					<?php echo $content = apply_filters ("the_content", $post->post_content); ?>
				</div>
				<div class="shared">
					<div class="newsletter">
						<div class="icon"></div>
						<h3 class="title">Rebeca as novas noticias no seu email</h3>
						<p class="description">Você tambem irá receber novidades de todo site</p>

						<form class="form" action="Post">
							<input type="email" class="input-email" placeholder="E-mail">
							<button class="btn-submit">Assinar</button>
						</form>

					</div>
					<!-- <div class="redes">
						<ul class="list">
							<li class="item facebook"></li>
							<li class="item linkedin"></li>
							<li class="item twitter"></li>
						</ul>
					</div> -->
				</div>
			</div>
		</div>
		<div class="relacionadas">
			<?php
				$args_posts = array(
					'post_type' => array('post'),
					'posts_per_page' => 2,
					'orderby' => 'rand'
				);

				$query_noticias = get_posts( $args_posts );

				query_posts( $args_posts );
			?>
			<ul class="list-news list">
				<?php 
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							get_template_part( 'components/news/new-card' );
						endwhile;
					endif;
				?>
			</ul>
		</div>
	</div>
<?php get_template_part( 'components/footer' ); ?>
<script type="text/javascript">
	
</script>