<?php get_template_part( 'components/header' ); ?>

<div class="section-page page-search">
	
	<?php
		global $query_string;
		$query_args = explode("&", $query_string);
		$search_query = array();

		foreach($query_args as $key => $string) {
			$query_split = explode("=", $string);
			$search_query[$query_split[0]] = urldecode($query_split[1]);
		} // foreach

		$the_query = new WP_Query($search_query);
		if ( $the_query->have_posts() ) : 
		?>
		<!-- the loop -->
		<div class="header-search">
			<div class="title-search">Encontramos <strong><?php echo $wp_query->post_count ?></strong> resultados para "<strong><?php echo wp_specialchars($wp_query->query_vars['s'], 1) ?></strong>"</div>
		</div>
		<ul class="list-results">
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<li class="result">
					<a href="<?php the_permalink(); ?>">
						<span class="title-result"><?php the_title(); ?></span>
						<span class="link-result"><?php the_permalink(); ?></span>
						<?php if ( has_post_thumbnail()) : ?>
							<?php $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
							<img class="thumb" src="<?php echo $thumbnail[0]; ?>"></img>
							<?php else : ?>
						<?php endif; ?>
						<span class="link-description"><?php echo the_excerpt_max_charlength(150) ?></span>
					</a>
				</li>
			<?php endwhile; ?>
		</ul>
		<!-- end of the loop -->

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<div class="not-found-search">
			<h3 class="title">Nenhum resultado da sua pesquisa foi encontrado</h3>
			<span class="description">A sua pesquisa <strong><?php echo wp_specialchars($wp_query->query_vars['s'], 1) ?></strong> não encontrou nenhum conteúdo relacionado ao portal correspondente</span>

			<div class="list-sugestoes">
				<span class="title-list">Sugestões:</span>

				<ul class="list">
					<li class="item">Tenha certeza de que todas as palavras estão escritas corretamente.</li>
					<li class="item">Tente palavras-chave diferentes.</li>
					<li class="item">Tente menos palavras-chave.</li>
				</ul>
			</div>
		</div>
		<!-- <p><?php _e( 'Nada encontrado :/' ); ?></p>  -->
	<?php endif; ?>
</div>

<?php get_template_part( 'components/footer' ); ?>